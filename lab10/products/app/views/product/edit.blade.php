@extends('../layout')
@section('title')
Edit | {{{ $product->name }}}
@stop
@section('content')
<h2>Edit {{{ $product->name }}}</h2>
{{Form::model($product, array('method' => 'PUT', 'route' => 
array('product.update', $product->id)));}}
{{ Form::label('name', 'Product Name: ') }}<br/>
{{ $errors->first('name') }}<br/>
{{ Form::text('name') }}<br/>
{{ Form::label('price', 'Price: ') }}<br/>
{{ $errors->first('price') }}<br/>
{{ Form::text('price') }}<br/>
{{ Form::submit('Update') }}
{{ Form::close() }}
@stop