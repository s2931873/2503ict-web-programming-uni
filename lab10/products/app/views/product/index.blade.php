@extends('../layout')
@section('title')
Products
@stop
@section('content')
<h2>Our Products</h2>
<ul>
 @foreach ($products as $product)
 <li> {{ link_to_route('product.show', $product->name, array($product->id)) }}
 @endforeach
</ul>
@stop