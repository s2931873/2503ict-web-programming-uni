<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";

/* Search sample data for $name or $year or $state from form. */
function search($input) {
  global $pms; 
  
  // Filter $pms by $name
  if (!empty($input))  {
    $results = array();
    foreach ($pms as $pm) {
      if (stripos($pm['name'], $input) !== FALSE && strlen($input) >= 3) {
        $results[] = $pm;
      }
       else if (strpos($pm['from'], $input) !== FALSE || 
          strpos($pm['to'], $input) !== FALSE) {
        $results[] = $pm;
       }
      else if (stripos($pm['state'], $input) !== FALSE && strlen($input) >= 3) {
        $results[] = $pm;
      
      }
    }
    $pms = $results;
  return $pms;
  }else{
  
  }
  
  
 
}
?>