<?php /* Smarty version Smarty-3.1.16, created on 2014-04-09 00:33:21
         compiled from "./templates/results.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1160691991534494207e0417-43611133%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bb8a6209a0e8563c0a055e398e0bc7b378fc0d87' => 
    array (
      0 => './templates/results.tpl',
      1 => 1397003600,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1160691991534494207e0417-43611133',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534494208bb150_77440669',
  'variables' => 
  array (
    'query' => 0,
    'pms' => 0,
    'pm' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534494208bb150_77440669')) {function content_534494208bb150_77440669($_smarty_tpl) {?><!DOCTYPE html>
<!-- Results page of associativprime minister search example. -->
<html>
<head>
  <title>Associative array search results page</title>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="styles/wp.css">
</head>

<body>
  <header>
  </header>

  <section>
  <h2>Australian Prime Ministers</h2>
  <h3>Results for '<?php echo $_smarty_tpl->tpl_vars['query']->value;?>
'</h3>

  <?php if ((count($_smarty_tpl->tpl_vars['pms']->value)==0)) {?> 
    <p>No results found.</p>
  <?php } else { ?>
    <table class="bordered">
    <thead>
    <tr><th>No.</th><th>Name</th><th>From</th><th>To</th><th>Duration</th><th>Party</th><th>State</th></tr>
    </thead>
    <tbody>
    <?php  $_smarty_tpl->tpl_vars['pm'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['pm']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['pms']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['pm']->key => $_smarty_tpl->tpl_vars['pm']->value) {
$_smarty_tpl->tpl_vars['pm']->_loop = true;
?>
    
      <tr><td><?php echo $_smarty_tpl->tpl_vars['pm']->value['number'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['pm']->value['name'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['pm']->value['start'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['pm']->value['finish'];?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['pm']->value['duration'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['pm']->value['party'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['pm']->value['state'];?>
</td></tr>
    <?php } ?>
    </tbody>
    </table>
  <?php }?>

  <p><a href="index.php">New search</a></p>
  </section>

  <footer>
  </footer>
</body>
</html>
<?php }} ?>
