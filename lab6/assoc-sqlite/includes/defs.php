<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "db_defs.php";

/* Search sample data for form data $query in name, year or state. */
function search($query) {
  global $pms; 
  $query = trim($query);
  $db = db_open();
  $query = "%$query%";
  $stmt= $db->prepare("SELECT * FROM pms WHERE name LIKE :query OR
  number LIKE :query OR party LIKE :query OR duration LIKE :query
  OR state LIKE :query OR start LIKE :query OR finish LIKE :query");
  $stmt->bindParam(':query', $query,  PDO::PARAM_STR);
  $stmt->execute();
$pms = $stmt->fetchAll();
  
    return $pms;
  
}
?>
