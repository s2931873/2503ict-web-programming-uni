@extends('../layout')
@section('title')
Products
@stop
@section('content')
<h2>Our Products</h2>
<ul>
 @foreach ($products as $product)
 <li>{{{ $product->name }}} - ${{{ $product->price }}}</li>
  
 @endforeach
</ul>
@stop