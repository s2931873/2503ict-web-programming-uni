@extends('../layout')
@section('title')
{{{ $product->name }}}
@stop
@section('content')
<h2>{{{ $product->name }}}</h2>
  <p>Price: ${{{ $product->price }}}</p>
@stop