<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
  return View::make('user.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
     $input = Input::all();
    $v = Validator::make($input, User::$rules);
     if ($v->passes()){
     $password = $input['password'];
     $encrypted = Hash::make($password);
     $user = new User;
     $user->username = $input['username'];
     $user->password = $encrypted;
     $user->remember_token = "default";
     $user->save();
       return Redirect::route('product.index');
     }else{
       return Redirect::route('user.create')->WithErrors($v);
       
     }
	}

  /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
  public function login(){
    
		// run the validation rules on the inputs from the form


		// if the validator fails, redirect back to the form
	
    $credentials = array(
  'username' => Input::get('username'),
  'password' => Input::get('password')
  );
   if (Auth::attempt($credentials)) { 
    return Redirect::to(URL::previous());
   }
    else{
    return Redirect::back()->withErrors('Your username or password was incorect.');
   }
  }
  /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
  public function logout(){
    Auth::logout();
      return Redirect::to(URL::previous());  
    }
  
  
  
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
