@extends('../layout')
@section('title')
Create User
@stop
@section('content')
@if (Auth::check())
    {{"You are already logged in"}}
@else
<h2>Create Account</h2>
{{Form::open(array('route' => 'user.store'))}}
  {{ Form::label("username", "Username") }}
  {{ Form::text("username") }}
  {{ Form::label("password", "Password") }}
  {{ Form::password("password") }}
  {{ Form::submit("Create") }}
  {{ Form::close() }}
@if($errors->has())
   @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
  @endforeach
@endif
@endif
@stop