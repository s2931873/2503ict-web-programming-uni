@extends('../layout')
@section('title')
{{{ $product->name }}}
@stop
@section('content')
<h2>{{{ $product->name }}}</h2>
  <p>Price: ${{{ $product->price }}}</p>
  <p>{{ link_to_route('product.edit', 'Edit', array($product->id)) }} </p>
@stop