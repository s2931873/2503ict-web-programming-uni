@extends('../layout')
@section('title')
Products
@stop
@section('content')

@if($errors->has())
   @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
  @endforeach
@endif

<h2>Our Products</h2>
<ul>
 @foreach ($products as $product)
 <li> {{ link_to_route('product.show', $product->name, array($product->id)) }}
 @endforeach
</ul>
@stop