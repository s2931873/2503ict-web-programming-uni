<html>
<head>
 <title>@yield('title')</title>
</head>
<body>
  
  @if (Auth::check())
    {{"You are logged in"}}
    {{ Auth::user()->username }}
  <br/>
    {{ link_to_route('user.logout', 'Log Out') }}

  @else
{{Form::open(array('route' => 'user.login'))}}
  {{ Form::label("username", "Username") }}
  {{ Form::text("username") }}<br/>
  {{ Form::label("password", "Password") }}
  {{ Form::password("password") }}<br/>
  {{ Form::submit("Login") }}
  {{ Form::close() }}  
  {{ link_to_route('user.create', 'Create an Account') }}
  @endif
  
 @yield('content')
  
</body>
</html>