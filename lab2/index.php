<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Job Search</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="style.css" rel="stylesheet" type="text/css"/>
  </head>
  <body>
    <div id="page">
      <div id="logo">
        <h1>JobSearch.com</h1>
      </div>
      <div id="nav">
        <div id="breadcrumbs">
          <p><a href="index.php">Home</a></p>
        </div>
      </div>
      <div id="content">
        
        <div id="right">
          <h2>Welcome to JobSearch.com</h2>
          <p>
          Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna
          aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
          aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat,
          vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum
          </p>
          <p>
          zzril delenit augue duis dolore te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil
          imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui
          facit eorum claritatem. Investigationes demonstraverunt lectores legere me lius quod ii legunt saepius. Claritas est etiam
          processus dynamicus, qui sequitur mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus
          parum claram, anteposuerit litterarum formas humanitatis per seacula quarta decima et quinta decima. Eodem modo typi,
          qui nunc nobis videntur parum clari, fiant sollemnes in futurum
          </p>
          <img id="job" src="job.jpg" alt="The place to find a job"/>
        </div>
        <div id="left">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="#">List All Jobs</a></li>
            <li><a href="#">Search for Jobs</a></li>
            <li><a href="">List All Employers</a></li>
            <li><a href="advertise.php">Advertise a job</a></li>
          </ul>
        </div>
      </div>
      <div id="footer">
        <p>Copyright JobSearch.com - 2013 - 2014</p>
      </div>
    </div>
    
  </body>
</html>