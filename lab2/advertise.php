<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>Advertise a Job</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="style.css" rel="stylesheet" type="text/css"/>
  </head>
  <body>
    <div id="page">
      <div id="logo">
        <h1>JobSearch.com</h1>
      </div>
      <div id="nav">
        <div id="breadcrumbs">
          <p><a href="index.php">Home</a> :: <a href="advertise.php">Advertise</a></p>
        </div>
      </div>
      <div id="content">
        
        <div id="right">
          <h2>Welcome to JobSearch.com</h2>
          
          <form action="submit_job.php" method="post">
            <label>Job Title:</label>
            <input name="title"/><br/>
            <label>Company:</label>
            <input name="company"/><br/>
            <label>Salary:</label>
            <input name="salary"/><br/>
            <label>Closing Date:</label>
            <input name="closing-date"/><br/>
            <button>Submit</button>
                </form>
        
        </div>
        <div id="left">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="#">List All Jobs</a></li>
            <li><a href="#">Search for Jobs</a></li>
            <li><a href="">List All Employers</a></li>
            <li><a href="advertise.php">Advertise a job</a></li>
          </ul>
        </div>
      </div>
      <div id="footer">
        <p>Copyright JobSearch.com - 2013 - 2014</p>
      </div>
    </div>
    
  </body>
</html>