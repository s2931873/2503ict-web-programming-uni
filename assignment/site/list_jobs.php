<?php
date_default_timezone_set('UTC'); 
include 'includes/database.php';
$database = new database;

$jobs = $database->selectJobs();

include 'Smarty/libs/Smarty.class.php';
$smarty = new Smarty;
$smarty->assign('jobs', $jobs);
$smarty->assign('title', 'Job Listings');

$smarty->display("jobs.tpl");
?>