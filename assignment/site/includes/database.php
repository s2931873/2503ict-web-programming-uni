<?php class database{

function __construct(){
$this->create();
$this->conn();
}

function conn(){
try {
 $db = new PDO('sqlite:db/database.sqlite');
 $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 } catch (PDOException $e) {
 die("Error: " . $e->getMessage());
 }
 return $db;
}

function create(){
  try {
$sql = "create table if not exists Job (
 id integer not null primary key autoincrement,
 title varchar(40) not null,
company integer not null,
location varchar(50) not null,
description varchar(150) not null,
salary varchar(40) not null,
closingDate varchar(40) not null)
;";
  $result = $this->conn()->query($sql);
$sql = "create table if not exists Employer (
 id integer not null primary key autoincrement, 
company varchar(40) not null unique,
industry varchar(50) not null,
description varchar(150) not null);";
$result = $this->conn()->query($sql);
  }catch (PDOException $e) {
 die("Error: " . $e->getMessage());
  }
}

function insertJob($title, $company, $salary, $location, $description, $closingDate){
try {
 $db = $this->conn();
 
 $sql = "insert into Job (title, company, location, salary, description , closingDate ) " .
   "values (:title, :company, :location, :salary, :description , :closingDate)";
 $statement = $db->prepare($sql);
 $statement->bindValue(':title', $title);
 $statement->bindValue(':company', $company);
 $statement->bindValue(':salary', $salary);
  $statement->bindValue(':location', $location);
  $statement->bindValue(':description', $description);
  $statement->bindValue(':closingDate', $closingDate);
  $statement->execute();
 }catch (PDOException $e) {
 die("Error: " . $e->getMessage());
}  }
  
function selectJobs(){
$jobs = array();
  try
{
 $db = $this->conn();
 $sql = "select * from Job";
 $result = $db->query($sql);
 // Grab all of the rows as an array
return $jobs = $result->fetchAll();
}
catch(PDOException $e)
{
 die("Error: " . $e->getMessage());
}
}

function selectJob($id){
  $result = array();
  $db = $this->conn();
  $sql = "SELECT * FROM Job WHERE id = :id;";
  $statement = $db->prepare($sql);
   
 $statement->bindValue(':id', $id);

  $statement->execute();
  
  $result = $statement->fetch();
return $result;  
}  
  
function delete($id){
   try {
        $db = $this->conn();
 $query = "DELETE FROM Job WHERE id = :id";
            $statement = $db -> prepare($query);
            $statement -> BindParam('id', $id, PDO::PARAM_INT);
            $result = $statement -> execute();
       
 }catch (PDOException $e) {
   die("Error: " . $e->getMessage());
 } 
}
  
function my_jobs($company){
 try {
      $db = $this->conn();
   $results = array();
   $sql = "SELECT * FROM Job WHERE company = :company";


$statement = $db->prepare($sql);
   
 $statement->bindValue(':company', $company);

  $statement->execute();
 $results =  $statement->fetchAll();
 return $results;
   
 }catch (PDOException $e) {
   die("Error: " . $e->getMessage());
 }
}
  
function update($id, $title, $location, $salary, $description, $closing)
{
 try {
  
             $db = $this->conn();
             $query = "UPDATE Job SET title = :title, location = :location,
             salary = :salary, closingDate = :closing, description = :description 
             WHERE id = :id;";
   
             $statement = $db -> prepare($query);
     
            $statement -> BindParam('id', $id, PDO::PARAM_INT);
   
            $statement -> BindValue(':title', $title);
            $statement -> BindValue(':location', $location);
            $statement -> BindValue(':salary', $salary);
            $statement -> BindValue(':closing', $closing);
            $statement -> BindValue(':description', $description);
            
   
            $statement -> execute();
       
 }catch (PDOException $e) {
   die("Error: " . $e->getMessage());
 }
}

function search($query){
 try {
   if($query != ""){
   $db = $this->conn();
   $results = array();
     $sql = "SELECT * FROM Job WHERE title LIKE :query OR company LIKE :query OR location LIKE :query OR salary LIKE :query OR closingDate LIKE :query";


$statement = $db->prepare($sql);
   $query = "%$query%";
 $statement->bindValue(':query', $query);

  $statement->execute();
 $results =  $statement->fetchAll();
 return $results;
   }
 }catch (PDOException $e) {
   die("Error: " . $e->getMessage());
 } 
}
 function insertEmployer($company, $industry, $description){
  try {
 $db = $this->conn();
 
 $sql = "insert into Employer (company, industry, description ) " .
   "values (:company, :industry, :description)";
 $statement = $db->prepare($sql);
 $statement->bindValue(':company', $company);
 $statement->bindValue(':industry', $industry);
 $statement->bindValue(':description', $description);

  $statement->execute();
 }catch (PDOException $e) {
 die("Error: " . $e->getMessage());
}  
  }
  function selectEmployers(){
 try {
  
   $db = $this->conn();
   $results = array();
   $sql = "SELECT * FROM Employer";


$statement = $db->prepare($sql);



  $statement->execute();
 $results =  $statement->fetchAll();
 return $results;
   
 }catch (PDOException $e) {
   die("Error: " . $e->getMessage());
 } 
}
  }
