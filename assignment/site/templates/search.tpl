{extends file='layout.tpl'}
{block name=title}Search{/block}
{block name=breadcrumb}
<a href="index.php">Home</a> :: <a href="search.php">Search</a>
{/block}
{block name=body}

<div id="right" class="col-xs-3 col-md-8 ">
{if isset($query)}
{foreach $query as $q}
<ul class="list-group">
  <li class="list-group-item">
{$q.title}
</li>
<li class="list-group-item">
{$q.company}
</li>
<li class="list-group-item">
{$q.salary}
</li>
<li class="list-group-item">
{$q.closingDate}
</li>
</ul>
<br/>
{/foreach}     
{/if}
<h2>Search</h2>
<P>Seacrh for a job title, company, salary, or closing date.</p>
<form class="navbar-form navbar-left" method="get" action="search.php"  role="search">
        <div class="form-group">
          <input name="query" type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <br/>

      </div>
      <div class="container-fluid">
      <div class="col-xs-3 col-md-8">
        <p>Copyright JobSearch.com - 2013 - 2014</p>
      </div>
    </div>
      </div>
      {/block}
</html>