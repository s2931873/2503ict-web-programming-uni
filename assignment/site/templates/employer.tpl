{extends file='layout.tpl'}
{block name=title}employer{/block}
{block name="extra-links"}
 <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.4.custom.css"/>
{/block}
{block name="extra-scripts"}
<script src="js/jquery-ui-1.10.4.custom.js"></script>
{/block}
{block name=breadcrumb}
<a href="index.php">Home</a> :: <a href="employer.php">Employer</a>
{/block}
{block name=body}

<div id="right" class="col-xs-3 col-md-8 ">
{if isset($employer)}
          <h2>Employers</h2>
          
          <ul class="list-group">
          
          {foreach $employer as $e}
         <li class="list-group-item">
          {$e.company}
          </li>
        
          <li class="list-group-item">
          Descripton: {$e.description}
           </li>
         <li class="list-group-item">
          Industry: {$e.industry}
           </li>
            <li class="list-group-item">
            <a href="employer.php?id={$e.id}">Enter employer area</a>
            </li>
          
          {/foreach}
          </ul>
          <br/>
          <p>Register as employer <a href="advertise.php">here</a></p>
          {/if}
          
          {if isset($job)}
          <h2>Welcome update, add or delete jobs here!</h2>
        
        {foreach $job as $j}
        <ul class="list-group">
         <li class="list-group-item">
          {$j.title}
          </li>
        
          <li class="list-group-item">
          Company: {$j.company}
           </li>
         <li class="list-group-item">
          Salary: {$j.salary}
           </li>
          <li class="list-group-item">
          Closing Date: {$j.closingDate}
           </li>
           <li class="list-group-item">
          Location: {$j.location}
           <li class="list-group-item">
          Description: {$j.description}
           </li>
              </ul>
            <a class="btn" href="employer.php?action=edit&amp;id={$j.id}">Edit</a>
            <a class="btn" href="delete.php?id={$j.id}">Delete</a>
            
          {/foreach}
          <br/>
         <a class="btn" href="advertise.php">Add Job</a>           
       
         <br/>
         {/if}
          {if isset($edit)}
             
        <script>
        $(function() {
        $("#datepicker").datepicker();
        
        });
        </script>
          <form action="update.php?id={$edit.id}" method="post">
         <label>Job Title:</label><br/>
         <input name="title" value="{$edit.title}"/><br/>
         <label>Location: </label><br/>
         <input name="location" value="{$edit.location}"/><br/>
         <label>Salary: </label><br/>
         <input name="salary" value="{$edit.salary}"/><br/>
         <label>Closing Date: </label><br/>
         <input id="datepicker" name="closing" value="{$edit.closingDate}"/><br/>
         <label>Description: </label><br/>
         <textarea name="description">{$edit.description}</textarea><br/>
         <button>Submit</button>
         </form>

          {/if}
          
       </div>
       </div>
      {/block}
</html>