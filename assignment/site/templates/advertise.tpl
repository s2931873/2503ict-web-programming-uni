{extends file='layout.tpl'}
{block name=title}Advertise a job{/block}
{block name="extra-links"}
 <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.4.custom.css"/>
{/block}
{block name="extra-scripts"}
<script src="js/jquery-ui-1.10.4.custom.js"></script>
{/block}
{block name=breadcrumb}
<a href="index.php">Home</a> :: <a href="advertise.php">Advertise a job</a>
{/block}
{block name=body}  
            
        <div id="right" class="col-xs-3 col-md-8 ">
          
          <div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
         Employer register
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
        
           
            <form id="add-employer" action="add_employer.php" method="post" onsubmit="return validateEmployer()">
            <label>Company:</label><br/>
            
            <input name="company" class="input-xlarge" required=""/><br/>
            <label>Industry:</label><br/>
            <input name="industry" class="input-xlarge" required=""/><br/>
            <label>Description:</label><br/>
            <textarea name="description" class="input-xlarge" required=""></textarea><br/>
            <button class="btn btn-default">Submit</button>
                </form>
                <script>
              function validateEmployer()
              {
              var company=document.forms["add-employer"]["company"].value;
              var industry=document.forms["add-employer"]["industry"].value;
              var salary=document.forms["add-employer"]["salary"].value;
              var description=document.forms["add-employer"]["description"].value;
              
              if (company==null || company=="")
              {
              alert("Company field must be filled out");
              return false;
              }
              var x=document.forms["myForm"]["industry"].value;
              if (industry==null || industry=="")
              {
              alert("Industry field must be filled out");
              return false;
              }
              if (salary==null || salary=="")
              {
              alert("Salary must be filled out");
              return false;
              }
              if (description==null || description=="")
              {
              alert("Description must be filled out");
              return false;
              }
              }
                </script>
                 </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          Post a Job
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <script>
        $(function() {
        $("#datepicker").datepicker();
        
        });
                     </script>   
          <form id="add-job" action="add_job.php" method="post" onsubmit="return validateForm()">
            <label>Job Title:</label><br/>
            <input name="title" class="input-xlarge" required=""/><br/>
            <label>Company:</label><br/>
            <select name="company">
            {foreach $employer as $e}
            <option value="{$e.id}">{$e.company}</option>
            {/foreach}
            </select><br/>
            <label>Salary:</label><br/>
            <input name="salary" class="input-xlarge" required=""/><br/>
            <label>Location:</label><br/>
            <input name="location" class="input-xlarge" required=""/><br/>
            <label>Description</label><br/>
            <textarea name="description"></textarea><br/>
            <label>Closing Date:</label><br/>
            <input name="closing-date" class="input-xlarge" id="datepicker" required=""/><br/>
            <button class="btn btn-default">Submit</button>
                </form>
                
<script>
  function validateForm()
  {
  //validation
  var title=document.forms["add-job"]["title"].value;
  var company=document.forms["add-job"]["company"].value;
  var salary=document.forms["add-job"]["salary"].value;
  var location=document.forms["add-job"]["location"].value;
  var closing=document.forms["add-job"]["closing-date"].value;
  
  if (title==null || title=="")
  {
  alert("Job title must be filled out");
  return false;
  }
  if (company==null || company=="")
  {
  alert("Company must be filled out");
  return false;
  }
  if (salary==null || salary=="")
  {
  alert("Salary must be filled out");
  return false;
  }
  if (location==null || location=="")
  {
  alert("Loccation must be filled out");
  return false;
  }
  if (closing==null || closing=="")
  {
  alert("Closing Date must be filled out");
  return false;
  }
  }  
</script>
</div>
    </div>
  </div>
  </div>
        </div>
        </div>
 {/block}


