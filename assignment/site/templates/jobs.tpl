{extends file='layout.tpl'}
{block name=title}Home{/block}
{block name=breadcrumb}
<a href="index.php">Home</a>
{/block}
{block name=body}

<div id="right" class="col-xs-3 col-md-8 ">       
          <h2>Jobs</h2>
          <ul class="list-group">
          
          {foreach $jobs as $job}
         <li class="list-group-item">
          {$job.title}
          </li>
        
          <li class="list-group-item">
          Company: {$job.company}
           </li>
         <li class="list-group-item">
          Salary: {$job.salary}
           </li>
          <li class="list-group-item">
          Closing Date: {$job.closingDate}
           </li>
           <li class="list-group-item">
          Location: {$job.location}
           <li class="list-group-item">
          Description: {$job.description}
           </li>
            <li class="list-group-item">
            <button>Apply for Job</button>
            </li>
          {/foreach}
          </ul>
       </div>
       </div>
      {/block}
</html>