<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
   <title>{block name=title}Default Page Title{/block}</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    {block name="extra-links"}{/block}
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    {block name="extra-scripts"}{/block}
  </head>
  <body>
<div class="container">
      <header id="logo">
      <div class="jumbotron ">
        <h1>Aero Jobs</h1>
      <p>The place for jobs!</p>
      </div>
      
        <div class="col-xs-3 col-md-8 " id="breadcrumbs">
          <p>{block name=breadcrumb}
          <a href="index.php">Home</a>
          {/block}</p>
          </div>
       <nav class="navbar navbar-default" role="navigation">
       
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
         <li><a href="index.php">Home</a></li>
            <li><a href="list_jobs.php">List All Jobs</a></li>
            <li><a href="employer.php">List All Employers</a></li>
            <li><a href="advertise.php">Advertise a job</a></li>
            <li><a href="documentation.html">Documentation</a></li>
      </ul>
      <form class="navbar-form navbar-left" method="get" action="search.php"  role="search">
        <div class="form-group">
          <input name="query" type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
      
        
      </header>
      
      <div class="container-fluid">
        <div id="left" class="col-sm-3 col-md-4 ">
          <ul class="nav nav-pills nav-stacked well">
            <li><a href="index.php"><span class="glyphicon glyphicon-home"> </span>Home</a></li>
            <li><a href="list_jobs.php"><span class="glyphicon glyphicon-list-alt"> </span>List All Jobs</a></li>
            <li><a href="search.php"><span class="glyphicon glyphicon-search"> </span>Search for Jobs</a></li>
            <li><a href=""><span class="glyphicon glyphicon-align-justify"> </span>List All Employers</a></li>
            <li><a href="advertise.php"><span class="glyphicon glyphicon-plus"> </span>Advertise a job</a></li>
            <li><a href="documentation.html">Documentation</a></li>
          </ul>
        </div>

  {block name=body}{/block} 
 <div class="container-fluid">
       <div class="col-xs-3 col-md-12 well" id="footer">
        <p>Copyright AeroJobs.com - 2013 - 2014</p>
        <p>Created by Matthew de Marillac | s2931873</p>
      </div>
  </div>
  </div>
  </body>
  </html>