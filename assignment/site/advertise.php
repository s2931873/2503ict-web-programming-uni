<?php
date_default_timezone_set('UTC'); 
include 'includes/database.php';
include 'Smarty/libs/Smarty.class.php';
$smarty = new Smarty;
$database = new database;
$employers = $database->selectEmployers();
$smarty->assign('title', 'Advertise a job');
$smarty->assign('employer', $employers);
$smarty->display("advertise.tpl");

?>