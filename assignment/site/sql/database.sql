create table if not exists Job (
 id integer not null primary key autoincrement,
 title varchar(40) not null,
company integer not null,
location varchar(50) not null,
description varchar(150) not null,
salary varchar(40) not null,
closingDate varchar(40) not null);

create table if not exists Employer (
 id integer not null primary key autoincrement, 
company varchar(40) not null unique,
industry varchar(50) not null,
description varchar(150) not null);

