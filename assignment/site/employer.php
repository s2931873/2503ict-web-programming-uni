<?php
date_default_timezone_set('UTC'); 
include 'includes/database.php';
$database = new database;

include 'Smarty/libs/Smarty.class.php';
$smarty = new Smarty;
if(isset($_GET['id']) && !isset($_GET['action'])){
  
  $jobs = $database->my_jobs($_GET['id']);
  $smarty->assign('job', $jobs);
  $smarty->assign('title', 'My Jobs');
  
}else if(isset($_GET['action']) && $_GET['action'] == "edit"){
 
  $edit = $database->selectJob($_GET['id']);
  $smarty->assign('edit', $edit);
  $smarty->assign('title', 'Edit Job');
}
else{
  
$employers = $database->selectEmployers();
$smarty->assign('employer', $employers);
$smarty->assign('title', 'Employers');
  
}

$smarty->display("employer.tpl");

?>