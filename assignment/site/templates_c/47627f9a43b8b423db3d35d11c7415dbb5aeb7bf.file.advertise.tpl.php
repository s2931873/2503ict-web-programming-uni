<?php /* Smarty version Smarty-3.1.16, created on 2014-04-25 14:09:08
         compiled from "./templates/advertise.tpl" */ ?>
<?php /*%%SmartyHeaderCode:75680343553321c19ed7bf2-53886674%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47627f9a43b8b423db3d35d11c7415dbb5aeb7bf' => 
    array (
      0 => './templates/advertise.tpl',
      1 => 1398432197,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398433235,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '75680343553321c19ed7bf2-53886674',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53321c19f1f1d9_90350171',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53321c19f1f1d9_90350171')) {function content_53321c19f1f1d9_90350171($_smarty_tpl) {?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
   <title>Advertise a job</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    
 <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.4.custom.css"/>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    
<script src="js/jquery-ui-1.10.4.custom.js"></script>

  </head>
  <body>
<div class="container">
      <header id="logo">
      <div class="jumbotron ">
        <h1>Aero Jobs</h1>
      <p>The place for jobs!</p>
      </div>
      
        <div class="col-xs-3 col-md-8 " id="breadcrumbs">
          <p>
<a href="index.php">Home</a> :: <a href="advertise.php">Advertise a job</a>
</p>
          </div>
       <nav class="navbar navbar-default" role="navigation">
       
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
         <li><a href="index.php">Home</a></li>
            <li><a href="list_jobs.php">List All Jobs</a></li>
            <li><a href="employer.php">List All Employers</a></li>
            <li><a href="advertise.php">Advertise a job</a></li>
            <li><a href="documentation.html">Documentation</a></li>
      </ul>
      <form class="navbar-form navbar-left" method="get" action="search.php"  role="search">
        <div class="form-group">
          <input name="query" type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
      
        
      </header>
      
      <div class="container-fluid">
        <div id="left" class="col-sm-3 col-md-4 ">
          <ul class="nav nav-pills nav-stacked well">
            <li><a href="index.php"><span class="glyphicon glyphicon-home"> </span>Home</a></li>
            <li><a href="list_jobs.php"><span class="glyphicon glyphicon-list-alt"> </span>List All Jobs</a></li>
            <li><a href="search.php"><span class="glyphicon glyphicon-search"> </span>Search for Jobs</a></li>
            <li><a href=""><span class="glyphicon glyphicon-align-justify"> </span>List All Employers</a></li>
            <li><a href="advertise.php"><span class="glyphicon glyphicon-plus"> </span>Advertise a job</a></li>
            <li><a href="documentation.html">Documentation</a></li>
          </ul>
        </div>

    
            
        <div id="right" class="col-xs-3 col-md-8 ">
          
          <div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
         Employer register
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">
      <div class="panel-body">
        
           
            <form id="add-employer" action="add_employer.php" method="post" onsubmit="return validateEmployer()">
            <label>Company:</label><br/>
            
            <input name="company" class="input-xlarge" required=""/><br/>
            <label>Industry:</label><br/>
            <input name="industry" class="input-xlarge" required=""/><br/>
            <label>Description:</label><br/>
            <textarea name="description" class="input-xlarge" required=""></textarea><br/>
            <button class="btn btn-default">Submit</button>
                </form>
                <script>
              function validateEmployer()
              {
              var company=document.forms["add-employer"]["company"].value;
              var industry=document.forms["add-employer"]["industry"].value;
              var salary=document.forms["add-employer"]["salary"].value;
              var description=document.forms["add-employer"]["description"].value;
              
              if (company==null || company=="")
              {
              alert("Company field must be filled out");
              return false;
              }
              var x=document.forms["myForm"]["industry"].value;
              if (industry==null || industry=="")
              {
              alert("Industry field must be filled out");
              return false;
              }
              if (salary==null || salary=="")
              {
              alert("Salary must be filled out");
              return false;
              }
              if (description==null || description=="")
              {
              alert("Description must be filled out");
              return false;
              }
              }
                </script>
                 </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
          Post a Job
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
      <div class="panel-body">
        <script>
        $(function() {
        $("#datepicker").datepicker();
        
        });
                     </script>   
          <form id="add-job" action="add_job.php" method="post" onsubmit="return validateForm()">
            <label>Job Title:</label><br/>
            <input name="title" class="input-xlarge" required=""/><br/>
            <label>Company:</label><br/>
            <select name="company">
            <?php  $_smarty_tpl->tpl_vars['e'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['e']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['employer']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['e']->key => $_smarty_tpl->tpl_vars['e']->value) {
$_smarty_tpl->tpl_vars['e']->_loop = true;
?>
            <option value="<?php echo $_smarty_tpl->tpl_vars['e']->value['id'];?>
"><?php echo $_smarty_tpl->tpl_vars['e']->value['company'];?>
</option>
            <?php } ?>
            </select><br/>
            <label>Salary:</label><br/>
            <input name="salary" class="input-xlarge" required=""/><br/>
            <label>Location:</label><br/>
            <input name="location" class="input-xlarge" required=""/><br/>
            <label>Description</label><br/>
            <textarea name="description"></textarea><br/>
            <label>Closing Date:</label><br/>
            <input name="closing-date" class="input-xlarge" id="datepicker" required=""/><br/>
            <button class="btn btn-default">Submit</button>
                </form>
                
<script>
  function validateForm()
  {
  //validation
  var title=document.forms["add-job"]["title"].value;
  var company=document.forms["add-job"]["company"].value;
  var salary=document.forms["add-job"]["salary"].value;
  var location=document.forms["add-job"]["location"].value;
  var closing=document.forms["add-job"]["closing-date"].value;
  
  if (title==null || title=="")
  {
  alert("Job title must be filled out");
  return false;
  }
  if (company==null || company=="")
  {
  alert("Company must be filled out");
  return false;
  }
  if (salary==null || salary=="")
  {
  alert("Salary must be filled out");
  return false;
  }
  if (location==null || location=="")
  {
  alert("Loccation must be filled out");
  return false;
  }
  if (closing==null || closing=="")
  {
  alert("Closing Date must be filled out");
  return false;
  }
  }  
</script>
</div>
    </div>
  </div>
  </div>
        </div>
        </div>
  
 <div class="container-fluid">
       <div class="col-xs-3 col-md-12 well" id="footer">
        <p>Copyright AeroJobs.com - 2013 - 2014</p>
        <p>Created by Matthew de Marillac | s2931873</p>
      </div>
  </div>
  </div>
  </body>
  </html><?php }} ?>
