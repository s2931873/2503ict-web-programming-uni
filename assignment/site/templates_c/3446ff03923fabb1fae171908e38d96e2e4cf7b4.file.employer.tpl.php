<?php /* Smarty version Smarty-3.1.16, created on 2014-04-25 14:09:10
         compiled from "./templates/employer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9501302015348ce41067c30-90272043%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3446ff03923fabb1fae171908e38d96e2e4cf7b4' => 
    array (
      0 => './templates/employer.tpl',
      1 => 1398432103,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398433235,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9501302015348ce41067c30-90272043',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_5348ce410ecfc5_31451081',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5348ce410ecfc5_31451081')) {function content_5348ce410ecfc5_31451081($_smarty_tpl) {?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
   <title>employer</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    
 <link rel="stylesheet" type="text/css" href="css/jquery-ui-1.10.4.custom.css"/>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    
<script src="js/jquery-ui-1.10.4.custom.js"></script>

  </head>
  <body>
<div class="container">
      <header id="logo">
      <div class="jumbotron ">
        <h1>Aero Jobs</h1>
      <p>The place for jobs!</p>
      </div>
      
        <div class="col-xs-3 col-md-8 " id="breadcrumbs">
          <p>
<a href="index.php">Home</a> :: <a href="employer.php">Employer</a>
</p>
          </div>
       <nav class="navbar navbar-default" role="navigation">
       
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
         <li><a href="index.php">Home</a></li>
            <li><a href="list_jobs.php">List All Jobs</a></li>
            <li><a href="employer.php">List All Employers</a></li>
            <li><a href="advertise.php">Advertise a job</a></li>
            <li><a href="documentation.html">Documentation</a></li>
      </ul>
      <form class="navbar-form navbar-left" method="get" action="search.php"  role="search">
        <div class="form-group">
          <input name="query" type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
      
        
      </header>
      
      <div class="container-fluid">
        <div id="left" class="col-sm-3 col-md-4 ">
          <ul class="nav nav-pills nav-stacked well">
            <li><a href="index.php"><span class="glyphicon glyphicon-home"> </span>Home</a></li>
            <li><a href="list_jobs.php"><span class="glyphicon glyphicon-list-alt"> </span>List All Jobs</a></li>
            <li><a href="search.php"><span class="glyphicon glyphicon-search"> </span>Search for Jobs</a></li>
            <li><a href=""><span class="glyphicon glyphicon-align-justify"> </span>List All Employers</a></li>
            <li><a href="advertise.php"><span class="glyphicon glyphicon-plus"> </span>Advertise a job</a></li>
            <li><a href="documentation.html">Documentation</a></li>
          </ul>
        </div>

  

<div id="right" class="col-xs-3 col-md-8 ">
<?php if (isset($_smarty_tpl->tpl_vars['employer']->value)) {?>
          <h2>Employers</h2>
          
          <ul class="list-group">
          
          <?php  $_smarty_tpl->tpl_vars['e'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['e']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['employer']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['e']->key => $_smarty_tpl->tpl_vars['e']->value) {
$_smarty_tpl->tpl_vars['e']->_loop = true;
?>
         <li class="list-group-item">
          <?php echo $_smarty_tpl->tpl_vars['e']->value['company'];?>

          </li>
        
          <li class="list-group-item">
          Descripton: <?php echo $_smarty_tpl->tpl_vars['e']->value['description'];?>

           </li>
         <li class="list-group-item">
          Industry: <?php echo $_smarty_tpl->tpl_vars['e']->value['industry'];?>

           </li>
            <li class="list-group-item">
            <a href="employer.php?id=<?php echo $_smarty_tpl->tpl_vars['e']->value['id'];?>
">Enter employer area</a>
            </li>
          
          <?php } ?>
          </ul>
          <br/>
          <p>Register as employer <a href="advertise.php">here</a></p>
          <?php }?>
          
          <?php if (isset($_smarty_tpl->tpl_vars['job']->value)) {?>
          <h2>Welcome update, add or delete jobs here!</h2>
        
        <?php  $_smarty_tpl->tpl_vars['j'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['j']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['job']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['j']->key => $_smarty_tpl->tpl_vars['j']->value) {
$_smarty_tpl->tpl_vars['j']->_loop = true;
?>
        <ul class="list-group">
         <li class="list-group-item">
          <?php echo $_smarty_tpl->tpl_vars['j']->value['title'];?>

          </li>
        
          <li class="list-group-item">
          Company: <?php echo $_smarty_tpl->tpl_vars['j']->value['company'];?>

           </li>
         <li class="list-group-item">
          Salary: <?php echo $_smarty_tpl->tpl_vars['j']->value['salary'];?>

           </li>
          <li class="list-group-item">
          Closing Date: <?php echo $_smarty_tpl->tpl_vars['j']->value['closingDate'];?>

           </li>
           <li class="list-group-item">
          Location: <?php echo $_smarty_tpl->tpl_vars['j']->value['location'];?>

           <li class="list-group-item">
          Description: <?php echo $_smarty_tpl->tpl_vars['j']->value['description'];?>

           </li>
              </ul>
            <a class="btn" href="employer.php?action=edit&amp;id=<?php echo $_smarty_tpl->tpl_vars['j']->value['id'];?>
">Edit</a>
            <a class="btn" href="delete.php?id=<?php echo $_smarty_tpl->tpl_vars['j']->value['id'];?>
">Delete</a>
            
          <?php } ?>
          <br/>
         <a class="btn" href="advertise.php">Add Job</a>           
       
         <br/>
         <?php }?>
          <?php if (isset($_smarty_tpl->tpl_vars['edit']->value)) {?>
             
        <script>
        $(function() {
        $("#datepicker").datepicker();
        
        });
        </script>
          <form action="update.php?id=<?php echo $_smarty_tpl->tpl_vars['edit']->value['id'];?>
" method="post">
         <label>Job Title:</label><br/>
         <input name="title" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['title'];?>
"/><br/>
         <label>Location: </label><br/>
         <input name="location" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['location'];?>
"/><br/>
         <label>Salary: </label><br/>
         <input name="salary" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['salary'];?>
"/><br/>
         <label>Closing Date: </label><br/>
         <input id="datepicker" name="closing" value="<?php echo $_smarty_tpl->tpl_vars['edit']->value['closingDate'];?>
"/><br/>
         <label>Description: </label><br/>
         <textarea name="description"><?php echo $_smarty_tpl->tpl_vars['edit']->value['description'];?>
</textarea><br/>
         <button>Submit</button>
         </form>

          <?php }?>
          
       </div>
       </div>
       
 <div class="container-fluid">
       <div class="col-xs-3 col-md-12 well" id="footer">
        <p>Copyright AeroJobs.com - 2013 - 2014</p>
        <p>Created by Matthew de Marillac | s2931873</p>
      </div>
  </div>
  </div>
  </body>
  </html><?php }} ?>
