<?php /* Smarty version Smarty-3.1.16, created on 2014-04-25 14:11:15
         compiled from "./templates/search.tpl" */ ?>
<?php /*%%SmartyHeaderCode:22054534953460e2d8091f0-94246448%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '692c3bc93f5ce1085954c77c1d0c047332ab27c2' => 
    array (
      0 => './templates/search.tpl',
      1 => 1398317933,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1398433235,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '22054534953460e2d8091f0-94246448',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53460e2d8c2811_80582732',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53460e2d8c2811_80582732')) {function content_53460e2d8c2811_80582732($_smarty_tpl) {?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
   <title>Search</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    
  </head>
  <body>
<div class="container">
      <header id="logo">
      <div class="jumbotron ">
        <h1>Aero Jobs</h1>
      <p>The place for jobs!</p>
      </div>
      
        <div class="col-xs-3 col-md-8 " id="breadcrumbs">
          <p>
<a href="index.php">Home</a> :: <a href="search.php">Search</a>
</p>
          </div>
       <nav class="navbar navbar-default" role="navigation">
       
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
         <li><a href="index.php">Home</a></li>
            <li><a href="list_jobs.php">List All Jobs</a></li>
            <li><a href="employer.php">List All Employers</a></li>
            <li><a href="advertise.php">Advertise a job</a></li>
            <li><a href="documentation.html">Documentation</a></li>
      </ul>
      <form class="navbar-form navbar-left" method="get" action="search.php"  role="search">
        <div class="form-group">
          <input name="query" type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
      
        
      </header>
      
      <div class="container-fluid">
        <div id="left" class="col-sm-3 col-md-4 ">
          <ul class="nav nav-pills nav-stacked well">
            <li><a href="index.php"><span class="glyphicon glyphicon-home"> </span>Home</a></li>
            <li><a href="list_jobs.php"><span class="glyphicon glyphicon-list-alt"> </span>List All Jobs</a></li>
            <li><a href="search.php"><span class="glyphicon glyphicon-search"> </span>Search for Jobs</a></li>
            <li><a href=""><span class="glyphicon glyphicon-align-justify"> </span>List All Employers</a></li>
            <li><a href="advertise.php"><span class="glyphicon glyphicon-plus"> </span>Advertise a job</a></li>
            <li><a href="documentation.html">Documentation</a></li>
          </ul>
        </div>

  

<div id="right" class="col-xs-3 col-md-8 ">
<?php if (isset($_smarty_tpl->tpl_vars['query']->value)) {?>
<?php  $_smarty_tpl->tpl_vars['q'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['q']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['query']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['q']->key => $_smarty_tpl->tpl_vars['q']->value) {
$_smarty_tpl->tpl_vars['q']->_loop = true;
?>
<ul class="list-group">
  <li class="list-group-item">
<?php echo $_smarty_tpl->tpl_vars['q']->value['title'];?>

</li>
<li class="list-group-item">
<?php echo $_smarty_tpl->tpl_vars['q']->value['company'];?>

</li>
<li class="list-group-item">
<?php echo $_smarty_tpl->tpl_vars['q']->value['salary'];?>

</li>
<li class="list-group-item">
<?php echo $_smarty_tpl->tpl_vars['q']->value['closingDate'];?>

</li>
</ul>
<br/>
<?php } ?>     
<?php }?>
<h2>Search</h2>
<P>Seacrh for a job title, company, salary, or closing date.</p>
<form class="navbar-form navbar-left" method="get" action="search.php"  role="search">
        <div class="form-group">
          <input name="query" type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <br/>

      </div>
      <div class="container-fluid">
      <div class="col-xs-3 col-md-8">
        <p>Copyright JobSearch.com - 2013 - 2014</p>
      </div>
    </div>
      </div>
       
 <div class="container-fluid">
       <div class="col-xs-3 col-md-12 well" id="footer">
        <p>Copyright AeroJobs.com - 2013 - 2014</p>
        <p>Created by Matthew de Marillac | s2931873</p>
      </div>
  </div>
  </div>
  </body>
  </html><?php }} ?>
