<?php /* Smarty version Smarty-3.1.16, created on 2014-04-10 12:45:11
         compiled from "./templates/login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:226238745534691480e69b0-18225485%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'f5f63cf8bf5077cbe9e40e023158dd20352e878a' => 
    array (
      0 => './templates/login.tpl',
      1 => 1397133873,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1397100426,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '226238745534691480e69b0-18225485',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_534691481a73c4_45295624',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_534691481a73c4_45295624')) {function content_534691481a73c4_45295624($_smarty_tpl) {?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
   <title>Home</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
  <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>    
  </head>
  <body>
<div class="container">
      <header id="logo">
      <div class="jumbotron ">
        <h1>JobSearch.com</h1>
      </div>
      
        <div class="col-xs-3 col-md-8 " id="breadcrumbs">
          <p>
<a href="index.php">Home</a>
</p>
          </div>
       <nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
         <li><a href="index.php">Home</a></li>
            <li><a href="list_jobs.php">List All Jobs</a></li>
            <li><a href="">List All Employers</a></li>
            <li><a href="advertise.php">Advertise a job</a></li>
        
      </ul>
      <form class="navbar-form navbar-left" method="get" action="search.php"  role="search">
        <div class="form-group">
          <input name="query" type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
      
        
      </header>
      
      <div class="container-fluid">
        <div id="left" class="col-sm-3 col-md-4 ">
          <ul class="nav nav-pills nav-stacked">
            <li><a href="index.php"><span class="glyphicon glyphicon-home"> </span>Home</a></li>
            <li><a href="list_jobs.php"><span class="glyphicon glyphicon-list-alt"> </span>List All Jobs</a></li>
            <li><a href="search.php"><span class="glyphicon glyphicon-search"> </span>Search for Jobs</a></li>
            <li><a href=""><span class="glyphicon glyphicon-align-justify"> </span>List All Employers</a></li>
            <li><a href="advertise.php"><span class="glyphicon glyphicon-plus"> </span>Advertise a job</a></li>
          </ul>
        </div>

  

<div id="right" class="col-xs-3 col-md-8 ">

          <h2>Login</h2>
          <form action="login.php?action=login" method="post">
          <label>User Name: </label><input name="username" /><br/>
          <label>Password: </label><input name="password" type="password"/><br/>
          <button>Submit</button><br/>
          </form>
       </div>
      <div class="container-fluid">
      <div class="col-xs-3 col-md-8">
        <p>Copyright JobSearch.com - 2013 - 2014</p>
      </div>
    </div>
      </div>
       
  </div>
  </body>
</html><?php }} ?>
