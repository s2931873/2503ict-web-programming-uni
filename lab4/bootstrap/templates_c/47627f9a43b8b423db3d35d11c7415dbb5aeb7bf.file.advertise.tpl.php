<?php /* Smarty version Smarty-3.1.16, created on 2014-04-01 23:52:44
         compiled from "./templates/advertise.tpl" */ ?>
<?php /*%%SmartyHeaderCode:75680343553321c19ed7bf2-53886674%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47627f9a43b8b423db3d35d11c7415dbb5aeb7bf' => 
    array (
      0 => './templates/advertise.tpl',
      1 => 1396396360,
      2 => 'file',
    ),
    '9e6b070c8cb75a2b091a59dcbc2131b5d5a97bf5' => 
    array (
      0 => './templates/layout.tpl',
      1 => 1396396177,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '75680343553321c19ed7bf2-53886674',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_53321c19f1f1d9_90350171',
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_53321c19f1f1d9_90350171')) {function content_53321c19f1f1d9_90350171($_smarty_tpl) {?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
   <title>Advertise a job</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
  <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>    
  </head>
  <body>
<div id="page" class="container-fluid">
      <div id="logo" class="row">
      <div class="col-xs-3 col-md-8 well ">
        <h1>JobSearch.com</h1>
      </div>
      
        <div class="col-xs-3 col-md-8 " id="breadcrumbs">
          <p>
<a href="index.php">Home</a> :: <a href="advertise.php">Advertise a job</a>
</p>
        </div>
      </div>
      <div class="row" id="content">
        <div id="left" class="col-sm-3 col-md-4 ">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="#">List All Jobs</a></li>
            <li><a href="#">Search for Jobs</a></li>
            <li><a href="">List All Employers</a></li>
            <li><a href="advertise.php">Advertise a job</a></li>
          </ul>
        </div>

      
      
        <div id="right" class="col-xs-3 col-md-8 ">
          <h2>Post a Job</h2>
      
          <h2>Welcome to JobSearch.com</h2>
          
          <form action="submit_job.php" method="post">
            <label>Job Title:</label><br/>
            <input name="title" class="input-xlarge" required=""/><br/>
            <label>Company:</label><br/>
            <input name="company" class="input-xlarge" required=""/><br/>
            <label>Salary:</label><br/>
            <input name="salary" class="input-xlarge" required=""/><br/>
            <label>Closing Date:</label><br/>
            <input name="closing-date" class="input-xlarge" required=""/><br/>
            <button class="btn btn-default">Submit</button>
                </form>
        </div>
        </div>
      <div class="row">
      <div class="col-xs-3 col-md-8 well">
        <p>Copyright JobSearch.com - 2013 - 2014</p>
      </div>
    </div>
      </div> 
  </body>
</html><?php }} ?>
