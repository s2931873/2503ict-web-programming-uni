{extends file='layout.tpl'}
{block name=title}Advertise a job{/block}
{block name=breadcrumb}
<a href="index.php">Home</a> :: <a href="advertise.php">Advertise a job</a>
{/block}
{block name=body}  
      
        <div id="right" class="col-xs-3 col-md-8 ">
          <h2>Post a Job</h2>
      
          <h2>Welcome to JobSearch.com</h2>
          
          <form action="submit_job.php" method="post">
            <label>Job Title:</label><br/>
            <input name="title" class="input-xlarge" required=""/><br/>
            <label>Company:</label><br/>
            <input name="company" class="input-xlarge" required=""/><br/>
            <label>Salary:</label><br/>
            <input name="salary" class="input-xlarge" required=""/><br/>
            <label>Closing Date:</label><br/>
            <input name="closing-date" class="input-xlarge" required=""/><br/>
            <button class="btn btn-default">Submit</button>
                </form>
        </div>
        </div>
      <div class="row">
      <div class="col-xs-3 col-md-8 well">
        <p>Copyright JobSearch.com - 2013 - 2014</p>
      </div>
    </div>
      </div>{/block}


