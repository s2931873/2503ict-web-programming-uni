<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
   <title>{block name=title}Default Page Title{/block}</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
  <link href="css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
    <link href="style.css" rel="stylesheet" type="text/css"/>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>    
  </head>
  <body>
<div id="page" class="container-fluid">
      <div id="logo" class="row">
      <div class="col-xs-3 col-md-8 well ">
        <h1>JobSearch.com</h1>
      </div>
      
        <div class="col-xs-3 col-md-8 " id="breadcrumbs">
          <p>{block name=breadcrumb}
          <a href="index.php">Home</a>
          {/block}</p>
        </div>
      </div>
      <div class="row" id="content">
        <div id="left" class="col-sm-3 col-md-4 ">
          <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="#">List All Jobs</a></li>
            <li><a href="#">Search for Jobs</a></li>
            <li><a href="">List All Employers</a></li>
            <li><a href="advertise.php">Advertise a job</a></li>
          </ul>
        </div>

  {block name=body}{/block} 
  </body>
</html>