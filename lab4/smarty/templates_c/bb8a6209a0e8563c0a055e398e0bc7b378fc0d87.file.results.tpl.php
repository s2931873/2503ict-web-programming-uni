<?php /* Smarty version Smarty-3.1.16, created on 2014-03-25 23:54:48
         compiled from "./templates/results.tpl" */ ?>
<?php /*%%SmartyHeaderCode:89386781353321142645d23-26439216%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bb8a6209a0e8563c0a055e398e0bc7b378fc0d87' => 
    array (
      0 => './templates/results.tpl',
      1 => 1395791686,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '89386781353321142645d23-26439216',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.16',
  'unifunc' => 'content_533211426baaa0_98571584',
  'variables' => 
  array (
    'query' => 0,
    'pms' => 0,
    'pm' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_533211426baaa0_98571584')) {function content_533211426baaa0_98571584($_smarty_tpl) {?><!DOCTYPE HTML>
<!-- Results page of associative array search example. -->
<html>
<head>
    <title>Associative array search results page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="styles/wp.css">
</head>

<body>
<h2>Results for '<?php echo htmlSpecialChars($_smarty_tpl->tpl_vars['query']->value);?>
'</h2>
<h3>Results</h3>

<?php if ((count($_smarty_tpl->tpl_vars['pms']->value)==0)) {?> 
  <p>No results found.</p>
<?php } else { ?>
  <table class="bordered">
  <thead>
  <tr><th>No.</th><th>Name</th><th>Address</th><th>Email</th><th>Phone</th></tr>
  </thead>
  <tbody>
  <?php  $_smarty_tpl->tpl_vars['pm'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['pm']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['pms']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['pm']->key => $_smarty_tpl->tpl_vars['pm']->value) {
$_smarty_tpl->tpl_vars['pm']->_loop = true;
?>
    <tr><td><?php echo $_smarty_tpl->tpl_vars['pm']->value['index'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['pm']->value['name'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['pm']->value['address'];?>
</td><td><?php echo $_smarty_tpl->tpl_vars['pm']->value['email'];?>
</td>
          <td><?php echo $_smarty_tpl->tpl_vars['pm']->value['phone'];?>
</td></tr>
  <?php } ?>
  </tbody>
  </table>
<?php }?>

<p><a href="index.php">New search</a></p>

<hr>
<p>
  Sources:
  <a href="show.php?file=results.php">results.php</a>
  <a href="show.php?file=templates/results.tpl">templates/results.tpl</a>
  <a href="show.php?file=includes/defs.php">includes/defs.php</a>
  <a href="show.php?file=includes/pms.php">includes/pms.php</a>
</p>

</body>
</html>
<?php }} ?>
