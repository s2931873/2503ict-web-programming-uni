<?php
/* Australian Prime Ministers.  Data as of 5 March 2010. */
$pms = array(
    array('index' => '1', 'name' => 'Edmund Barton', 'address' => '29 Smith Street', 'phone' => '55056687', 'email' => 'edmund@edmundmail.com'),
    array('index' => '2', 'name' => 'Alfred Deakin', 'address' => '24 John Street', 'phone' => '0413628610', 'email' => 'Alfred54@hotmail.com' ),
    array('index' => '3', 'name' => 'Chris Watson', 'address' => '27 April Drive', 'phone' => '4455889966', 'email' => 'chris@outlook.com'),
    array('index' => '4', 'name' => 'George Reid', 'address' => '18 Silverpine Street', 'phone' => '56465654464', 'email' => 'gorge@icloud.com'),
    array('index' => '-', 'name' => 'Alfred Deakin', 'address' => '7 End Street', 'phone' => '55297721', 'email' => 'siralfred@pm.com'),
);
?>
