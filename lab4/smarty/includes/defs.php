<?php
date_default_timezone_set('UTC'); 
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";

/* Search sample data for form data $query in name, year or state. */
function search($query) {
    global $pms; 

    // Filter $pms by $query
    if (!empty($query)) {
	$results = array();
	foreach ($pms as $pm) {
	    if (stripos($pm['name'], $query) !== FALSE ||
	        strpos($pm['address'], $query) !== FALSE || 
	        strpos($pm['phone'], $query) !== FALSE ||
	        stripos($pm['email'], $query) !== FALSE) {
		$results[] = $pm;
	    }
	}
	return $results;
    } else {
	return $pms;
    }
}
?>
