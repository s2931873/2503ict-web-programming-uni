<?php

class UserController extends \BaseController {

  //seeker create form
  public function create_seeker()
	{
  return View::make('user.seeker.create');
	}

  //employer create form
  public function create_employer()
	{
  return View::make('user.employer.create');
	}

	/**
	 * Store a newly created user/employer in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    $input = Input::all();
    $v = Validator::make($input, Employer::$rules);
    if($input['type'] == 2){
    if ($v->passes())
    {
      //if user is employer, we store employer details
       $employer = new Employer;
       $employer->company = $input['company'];
       $employer->industry = $input['industry'];
       $employer->description = $input['description'];
       $employer->remember_token = "default";
       $employer->save();
    }else{
      //employer rules bad
       return Redirect::to(URL::previous())->withErrors($v);
    }
  }
    //validate job seeker
    $e = Validator::make($input, User::$rules);
 if ($e->passes())
 {    //encrypt password one way
       $password = $input['password'];
       $encrypted = Hash::make($password);
       //save a regular user
       $user = new User;
       $user->username = $input['username'];
       $user->password = $encrypted;
       $user->email = $input['email'];
       $user->phone_number = $input['phone_number'];
       $user->image = $input['image'];
       $user->full_name = $input['full_name'];   
       $user->type= $input['type'];
       $user->remember_token = "default";
   //if user is employer create foreign key
    if($input['type'] == 2){
       $account = Employer::find($employer->id);
       $user->employer()->associate($account);
    }
       $user->save();  
   //auto log them in
    $credentials = array(
  'username' => Input::get('username'),
  'password' => Input::get('password')
  );
   Auth::attempt($credentials);
   //redirect to home page with jobs 
   $jobs = Job::take(7)->orderBy('id', 'DESC')->get();
   return Redirect::route('job.index', compact('jobs'));
 }else{
   //seeker validation fails
   return Redirect::to(URL::previous())->withErrors($e);
 }
     }
  
  /**
	 * Handle - User logs in
	 *
	 * @return Response
	 */
  public function login(){
    $credentials = array(
  'username' => Input::get('username'),
  'password' => Input::get('password')
  );
   if (Auth::attempt($credentials)) {
   
    return Redirect::to(URL::previous());
    }else{
     return Redirect::to(URL::previous())->withErrors('Sorry, The username or password was     incorrect!');
   }
  }
  
  /**
	 * Handle - User logs out.
	 *
	 * @return Response
	 */
  public function logout(){
      Auth::logout();
      return Redirect::to(URL::previous())->WithErrors("You have logged out");  
    }
  
  public function profile($id){
     $user = User::where('id', '=', $id)->get();
     return View::make('user.seeker.profile', compact('user'));
  }
}
