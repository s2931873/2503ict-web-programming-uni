<?php

class EmployerController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    //return a list of employers
		$employers = Employer::paginate(10);
    //get employers user details
    return View::make('employer.all', compact('employers'));
	}
  
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
    //return a list of employers jobs
     $job = Job::where('employer_id', '=', $id)->get();
     return View::make('employer.show', compact('job'))->With('employer_id', $id);
}
  
  public function applications($id, $employer_id)
  { 
    //seelect the user who applied for the aplication as well as its details
    $applications = DB::select("Select u.*, a.application_letter, a.application_date From jobs as j, users as u, applications as a Where j.id = ? And a.job_id = ? and j.id= a.job_id and u.id = a.seeker_id and j.employer_id = ?", array($id, $id, $employer_id));
    //return view
    //paginate results
    $applications = Paginator::make($applications, count($applications), 10);
    return View::make('employer.applications', compact('applications'))->With('employer_id', $employer_id);
  }
}