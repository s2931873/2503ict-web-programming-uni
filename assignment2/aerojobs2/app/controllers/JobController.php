<?php

class JobController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    //get seven most recent jobs
		$jobs = Job::take(7)->orderBy('id', 'DESC')->get();
    return View::make('job.index', compact('jobs'));
	}
  
/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function listall()
	{
    //if user is job seeker dont show expired jobs
    if (Auth::check()){
    if(Auth::user()->type == 1){
	  //sql get only jobs that haven't expired
      $jobs = Job::where('end_date', '>=', new DateTime('today'))->paginate(15);
    }else{
      //if user is employer show all jobs
    $jobs = Job::paginate(15);
  }
	}else{
   //if user is guest show only valid jobs
    	$jobs = Job::where('end_date', '>=', new DateTime('today'))->paginate(15);
    }
     return View::make('job.list', compact('jobs'));
  }
  
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	  return View::make('job.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
    //validation
    $v = Validator::make($input, Job::$rules);
    //lets make sure this is saved to the correct employer
    if(Auth::user()->employer_id == $input['employer_id']){
    //lets save the job
      if($v->passes()) {
    $job = new Job();
    $job->title = $input['title'];
    $job->location = $input['location'];
    $job->description = $input['description'];
    $job->salary = preg_replace("/[^0-9]/","",$input['salary']);
    $job->start_date = $input['start_date'];
    $job->end_date = $input['end_date'];
    $job->employer_id = $input['employer_id'];
    $job->save();
    return Redirect::route('job.show', $job->id);
      }else{
       return Redirect::action('JobController@create')->withErrors($v);
      }
  }else{
      //unfortunately the job was trying to be saved to the wrong employer
  return Redirect::route('job.create')->With("You are not saving to the correct employer,
  please do not change hidden inputs!");
  }
}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
    $jobs = Job::find($id);
    //get days left for job
    $datetime1 = date_create('now');
    $datetime2 = date_create($jobs->end_date);
    $interval = date_diff($datetime1, $datetime2);
    
     return View::make('job.show', compact('jobs'))->With('interval', $interval);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	$jobs = Job::find($id);
  return View::make('job.edit', compact('jobs'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
   
    $job = Job::find($id);
    $input = Input::all();
    $v = Validator::make($input, Job::$rules);
    //lets make sure this is saved to the correct employer
    if(Auth::user()->employer_id == $input['employer_id']){
    //lets save the job
      if($v->passes()) {
    $job->title = $input['title'];
    $job->location = $input['location'];
    $job->description = $input['description'];
    $job->salary = preg_replace("/[^0-9]/","",$input['salary']);
    $job->start_date = $input['start_date'];
    $job->end_date = $input['end_date'];
    $job->employer_id = $input['employer_id'];
    $job->save();
        return Redirect::route('job.show', $id)->withErrors("Update Succeded");
      }else{
      return Redirect::to(URL::previous())->withErrors($v);
      }
    }
  }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 $job = Job::find($id);
    //check this job belongs to user
    if(Auth::user()->employer_id == $job->employer_id){
     $job->delete();
   return Redirect::route('job.list');
        }
	}

  public function apply($id)
	{
		 $job = Job::find($id);
    //check this job belongs to user
     return View::make('job.apply', compact('job'));
  }
  
    public function application_store()
	  {
       $input = Input::all();
      
      //dynamic rules
      //seeker_id can only exist in one job_id
      //application letter ten characters min and required
      $rules = array(
     'seeker_id' => 'unique:applications,seeker_id,NULL,id,job_id,'.$input['id'].'',
     'application_letter' => 'required|min:10'
      );
      $id =  $input['id'];
      //validate
      $v = Validator::make($input, $rules);
      if($v->passes()) {
    //save appplication
    $application = new Application;
    $application->job_id = $input['id'];
    $application->seeker_id = $input['seeker_id'];
    $application->application_letter = $input['application_letter'];
    $application->application_date = Carbon::now();
    $application->remember_token = "default";
    $application->save();
    
    //send appropriate data and user to home page view    
    $jobs = Job::take(7)->orderBy('id', 'DESC')->get();    
    return Redirect::route('job.show', $id)->withErrors("Thanks for applying");
      }else{
       return Redirect::to(URL::previous())->withErrors($v);
      }
    }
    
  public function search(){
    $input = Input::all();
    //get query
    $query = $input['query'];
    //search for industry, description, location or salary
    $jobs = Job::whereRaw('salary LIKE ? OR title LIKE ? OR location LIKE ? OR description LIKE ?', array(preg_replace("/[^0-9]/","",'%'.$query.'%'), '%'.$query.'%', '%'.$query.'%', '%'.$query.'%'))
->paginate(2);
    //paginate
     
    
    return View::make('job.search', compact('jobs'));
}
}