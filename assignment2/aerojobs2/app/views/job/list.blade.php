@extends('../layout')
@section('title')
All Jobs| Aero Jobs - The place for jobs!
@stop
@section('body')
<h3>All Jobs</h3>
{{ $jobs->links()}}
 @foreach ($jobs as $job)

<ul class="list-group">
  <li class="list-group-item"><h4>{{ link_to_route('job.show',$job->title, array($job->id)) }}</h4></li>
  <li class="list-group-item">Location: {{{ $job->location }}}</li>
  <li class="list-group-item">Salary: {{{ $job->salary }}}</li>
  <li class="list-group-item">Start Date: {{{ $job->start_date }}}</li>
  <li class="list-group-item">End Date: {{{ $job->end_date }}}</li>
  <li class="list-group-item">{{ link_to_route('job.show','More info', array($job->id)) }}</li>
</ul>
<br/>

  @endforeach
{{ $jobs->links()}}
@stop
