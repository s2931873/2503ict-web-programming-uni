@extends('../layout')
@section('title')
Create Job
@stop
@section('body')

@if (Auth::check())
@if(Auth::user()->type == 1)
<h2>Apply for "{{{ $job->title }}}"</h2>
<p>Welcome, {{{ Auth::user()->full_name }}}! Apply for the job bellow!</p>
{{ Form::open(array('action' => 'JobController@application_store')) }}
{{ Form::label('application_letter', 'Application letter: ') }}<br/>
{{Form::textarea('application_letter')}}<br/>
{{Form::hidden('id', $job->id)}}
{{Form::hidden('seeker_id', Auth::user()->id)}}
{{ Form::submit('Create') }}
{{ Form::close() }}
@if($errors->has())
    @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
    @endforeach
    @endif
@else
{{"You must be logged in as a job seeker to view this page."}}

@endif
@else
{{"You must be logged in as a job seeker to view this page."}}

@endif
@stop