@extends('../layout')
@section('title')
{{{ $jobs->title }}} | Aero Jobs - The place for jobs!
@stop
@section('body')
  <ul>
   <li class="list-group-item"><h2>{{{ $jobs->title }}}</h2></li>
   <li class="list-group-item">Location: {{{ $jobs->location }}}</li>
   <li class="list-group-item">Description: <br/>
    {{{ $jobs->description }}}</li>
   <li class="list-group-item">Salary: {{{ $jobs->salary }}}</li>
   <li class="list-group-item">Days Left: {{$interval->format('%R%a days')}}</li>
   <li class="list-group-item">Start Date: {{{ $jobs->start_date }}}</li>
   <li class="list-group-item">End Date: {{{ $jobs->end_date }}}</li>
@if (Auth::check())

@if(Auth::user()->employer_id == $jobs->employer_id)
    <li class="list-group-item"><button>{{ link_to_route('job.edit','Edit Job!', array($jobs->id)) }}    </button>
      <br/>
      <br/>
    {{ Form::open(array('method' => 'DELETE', 'route' => array('job.destroy', $jobs->id))) }}
    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}</li>
   <li class="list-group-item"><button>{{ link_to_route('employer.applications','View job applications', array($jobs->id, $jobs->employer_id)) }}</button></li>
   </ul>
@endif
@if(Auth::user()->type != 2)
   <p><button>{{ link_to_route('job.apply','Apply for job!', array($jobs->id)) }}</button></p>
@endif
@else
   <p>You must be logged in to apply for a job.</p>
@endif
 @if($errors->has())
            @foreach ($errors->all() as $error)
              <div>{{ $error }}</div>
            @endforeach
            @endif
@stop