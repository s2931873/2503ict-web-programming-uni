@extends('../layout')
@section('title')
Edit | {{{ $jobs->title }}}
@stop
@section('extra_link')
    {{ HTML::style('css/jquery-ui-1.10.4.custom.css') }}
@stop
@section('extra_script')
  {{ HTML::script('js/jquery-ui-1.10.4.custom.js') }}
  <script>
  $(function() {
    $("#start_date").datepicker();
    $("#end_date").datepicker();
  });
  </script>
@stop
@section('body')

    @if (Auth::check())
    @if(Auth::user()->employer_id == $jobs->employer_id)
    <h2>Edit "{{{ $jobs->title }}}"</h2>
{{Form::model($jobs, array('method' => 'PUT', 'route' => 
  array('job.update', $jobs->id))) }}
  {{ Form::label('title', 'Job Ttile: ') }}
  {{ $errors->first('title') }}<br/>
  {{ Form::text('title') }}<br/>
{{ Form::label('location', 'Location:') }}
  {{ $errors->first('location') }}<br/>
  {{ Form::text('location') }}<br/>
  {{ Form::label('salary', 'Salary (format doesn\'t matter):') }}
  {{ $errors->first('salalry') }}<br/>
  {{ Form::text('salary') }}<br/>
  {{ Form::label('description', 'Description: ') }}
  {{ $errors->first('description') }}<br/>
  {{ Form::text('description') }}<br/>
  {{ Form::label('start_date', 'Start Date: ') }}
  {{ $errors->first('start_date') }}<br/>
  {{ Form::text('start_date') }}<br/>
  {{ Form::label('end_date', 'End Date: ') }}
  {{ $errors->first('end_date') }}<br/>
  {{ Form::text('end_date') }}<br/>
  {{ Form::hidden('employer_id', $jobs->employer_id) }}
  {{ Form::submit('Update') }}
  {{ Form::close() }}
 @if($errors->has())
    @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
    @endforeach
    @endif
    @endif
    @else
{{"Sorry, you are not authorized to view this page."}}

@endif
@stop