@extends('../layout')
@section('title')
Create Job
@stop
@section('extra_link')
    {{ HTML::style('css/jquery-ui-1.10.4.custom.css') }}
@stop
@section('extra_script')
  {{ HTML::script('js/jquery-ui-1.10.4.custom.js') }}
  <script>
  $(function() {
    $("#start_date").datepicker();
    $("#end_date").datepicker();
  });
  </script>
@stop
@section('body')


@if (Auth::check())
@if(Auth::user()->type == 2)
<p>Welcome, create a job bellow!</p>
{{ Form::open(array('action' => 'JobController@store')) }}
{{ Form::label('title', 'Job Tile: ') }}
{{ $errors->first('title') }}<br/>
{{ Form::text('title') }}<br/>
{{ Form::label('location', 'Location: ') }}
{{ $errors->first('location') }}<br/>
{{ Form::text('location') }}<br/>
{{ Form::label('salary', 'Salary (format doesn\'t matter): ') }}
{{ $errors->first('salalry') }}<br/>
{{ Form::text('salary') }}<br/>
{{ Form::label('description', 'Description: ') }}
{{ $errors->first('description') }}<br/>
{{ Form::text('description') }}<br/>
{{ Form::label('start_date', 'Start Date: ') }}
{{ $errors->first('start_date') }}<br/>
{{ Form::text('start_date') }}<br/>
{{ Form::label('end_date', 'End Date: ') }}
{{ $errors->first('end_date') }}<br/>
{{ Form::text('end_date') }}<br/>
{{ Form::hidden('employer_id',Auth::user()->employer_id ) }}
{{ Form::submit('Create') }}
{{ Form::close() }}
@else
{{"You must be logged in as an employer to view this page."}}

@endif
@else
{{"You must be logged in as an employer to view this page."}}

@endif
@stop