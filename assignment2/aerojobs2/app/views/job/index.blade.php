@extends('../layout')
@section('title')
Home page| Aero Jobs - The place for jobs!
@stop
@section('body')
<h3>Welcome</h3>
<p>
   Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer accumsan bibendum cursus. Nunc neque ante, consectetur placerat ante eget, facilisis     tincidunt sem. Aenean et quam et lectus condimentum laoreet at sit amet nisl. In eleifend commodo odio id condimentum. Suspendisse non erat vitae nibh pretium eleifend. Donec adipiscing venenatis mi ut mollis. Sed lectus velit, vestibulum ac libero et, faucibus ultricies sem. Aenean vehicula nunc a massa ultrices posuere. Donec eget eros suscipit, fermentum ipsum sit amet, sagittis urna. Cras eu elit velit. Maecenas quis odio eu nibh convallis accumsan. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam sollicitudin nulla at semper mattis. Phasellus ullamcorper ante ac dui aliquet euismod in non odio. In eros risus, egestas id bibendum vitae, malesuada eu tellus. Vestibulum faucibus congue pharetra.    
</p>
<h3>Recent Jobs</h3>

 @foreach ($jobs as $job)
<ul class="list-group">
  <li class="list-group-item"><h4>{{ link_to_route('job.show',$job->title, array($job->id)) }}</h4></li>
  <li class="list-group-item">Location: {{{ $job->location }}}</li>
  <li class="list-group-item">Salary: {{{ $job->salary }}}</li>
  <li class="list-group-item">Start Date: {{{ $job->start_date }}}</li>
  <li class="list-group-item">End Date: {{{ $job->end_date }}}</li>
  @if (Auth::check())
  @if(Auth::user()->type != 2)
  <li class="list-group-item"><button>{{ link_to_route('job.apply','Apply for job!', array($job->id)) }}</button></li>
  @endif
  @else
  <li class="list-group-item">You must be logged in to apply for a job</li>
  @endif
</ul>
<br/>
  @endforeach
@stop