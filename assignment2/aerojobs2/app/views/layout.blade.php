<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
   <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
    {{ HTML::style('css/bootstrap.css') }}
    {{ HTML::style('css/bootstrap-theme.css') }}
    {{ HTML::style('css/style.css') }}
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    @yield('extra_link')
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    {{ HTML::script('js/bootstrap.js') }}
     @yield('extra_script')
  </head>
  <body>
  <div class="container">
      <header id="logo">
      <div class="jumbotron ">
        <h1>Aero Jobs</h1>
      <p>The place for jobs!</p>
      </div>
        <hr/>
      </header>
      <div class="container-fluid">
        <div id="left" class="col-sm-3 col-md-4 ">
           
          <h3>Navigation</h3>
          <ul class="nav nav-pills nav-stacked well">
            <li>{{ link_to_route('job.index', 'Home') }}</li>
            <li>{{ link_to_route('job.list', 'All Jobs') }}</li>
            <li>{{ link_to_route('employer.all', 'All Employers') }}</li>
            @if (Auth::check())
            @if(Auth::user()->type == 2)
            <li>{{ link_to_route('job.create', 'Advertise a Job') }}</li>
            @endif
            @endif
            <li>{{ link_to('/docs/', 'Documentation') }}</li>
            <li>
            {{ Form::open(array('action' => 'JobController@search', 'method' => 'get' , 'class'=>'navbar-form navbar-left')) }}
            {{ Form::text('query') }}
            {{ Form::submit('Search') }}  
            {{ Form::close() }}
          </li>
          </ul>
            <div class="well">
             @if (Auth::check())
            <h3>My Account</h3>
          <ul style="padding:0px;">
          <li class="list-group-item">
          <img alt="My profile pic" title="{{ Auth::user()->full_name }}'s profile pic" id="login-thumb" src="{{           asset(Auth::user()->image->url('thumb')) }}">
         {{"You are logged in"}}
         {{ Auth::user()->full_name }} <br/> 
         {{ link_to_route('user.logout', 'Log Out') }}<br/>
         @if( Auth::user()->type == 2)
            <li class="list-group-item">{{ link_to_route('employer.show','Go to my admin area', array(Auth::user()->employer_id)) }}</li>
      @endif
            </ul>
      @else
             <h3>Login or Sign up</h3>
      {{Form::open(array('route' => 'user.login'))}}
        {{ Form::label("username", "Username") }}
        {{ Form::text("username") }}<br/>
        {{ Form::label("password", "Password") }}
        {{ Form::password("password") }}<br/>
        {{ Form::submit("Login") }}
        {{ Form::close() }}  <br/>
        {{ link_to_route('user.seeker.create', 'Create a Job Seeker Account') }}
         <br/>
          {{ link_to_route('user.employer.create', 'Create an Employer Account') }}
            @if($errors->has())
            @foreach ($errors->all() as $error)
              <div>{{ $error }}</div>
            @endforeach
            @endif
            @endif
          </div>
          <div>
            <h3>Recent Jobs</h3>
           @foreach ($recent as $job)
          <ul class="list-group">
              <li class="list-group-item"><h4>{{ link_to_route('job.show',$job->title, array($job->id)) }}</h4></li>
              <li class="list-group-item">Salary: {{{ $job->salary }}}</li>
              <li class="list-group-item">End Date: {{{ $job->end_date }}}</li>
          </ul>
          @endforeach
          </div>
        </div>
  <div id="right" class="col-sm-6 col-md-8 ">
      @yield('body')
  </div>
 <div class="container-fluid">
       <div class="col-xs-3 col-md-12 well" id="footer">
        <p>Copyright AeroJobs.com - 2013 - 2014</p>
        <p>Created by Matthew de Marillac | s2931873</p>
      </div>
  </div>
 </div>
</div>
</body>
  </html>