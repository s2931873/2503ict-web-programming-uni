@extends('../layout')
@section('title')
Documetation| Aero Jobs - The place for jobs!
@stop
@section('body')
<h2>Matthew de Marillac - s2931873</h2>
<h3>Aero Jobs Documentation</h3>
<h4>Schemas</h4>
<p>Jobs table</p>
  <code>
  Schema::create('jobs', <br/>
    function($table)<br/>
     {<br/>
       $table->increments('id');<br/>
       $table->string('title');<br/>
       $table->string('location');<br/>
       $table->text('description');<br/>
       $table->double('salary');<br/>
       $table->datetime('start_date');<br/>
       $table->datetime('end_date');<br/>
      $table->integer('employer_id');<br/>
       $table->timestamps();<br/>
     });
  </code>
<p>Employers table</p>
  <code>
  Schema::create('employers',<br/> 
    function($table)<br/>
     {<br/>
       $table->increments('id');<br/>
       $table->string('company')->unique();;<br/>
       $table->string('industry');<br/>
       $table->text('description');<br/>
       $table->string('remember_token');<br/>
       $table->timestamps();<br/>
     });
  </code>
<p>Users table</p>
  <code>
Schema::create('users',<br/>
    function($table) {<br/>
     $table->increments('id');<br/>
     $table->string('username')->unique();<br/>
     $table->string('password')->index();<br/>
     $table->string('email')->unique();<br/>
     $table->string('phone_number');<br/>
     $table->string('full_name');<br/>
     $table->integer('type');<br/>
     $table->integer('employer_id')->nullable();<br/>
     $table->string('remember_token');<br/>
     $table->timestamps();<br/>
    });<br/>
  </code>
<p>Applications table</p>
  <code>
  Schema::create('applications', <br/>
    function($table)<br/>
     {<br/>
       $table->increments('id');<br/>
       $table->integer('job_id');<br/>
       $table->integer('seeker_id');<br/>
       $table->string('application_letter');<br/>
       $table->datetime('application_date');<br/>
       $table->string('remember_token');<br/>
       $table->timestamps();<br/>
     });
  </code>
<h4>Demo Users</h4>
<p>Employers</p>
<ul>
  <li>
  Username : employerone<br/>
  Password: letmein
  </li>
  <li>
  Username : employertwo<br/>
  Password: letmein
  </li>
  <li>
  Username : employerthree<br/>
  Password: letmein
  </li>
</ul>
<p>Job Seekers</p>
<ul>
<li>
  Username : jobone<br/>
  Password: letmein
  </li>
<li>
  Username : jobtwo<br/>
  Password: letmein
  </li>
</ul>
<h4>How to use</h4>
  <p>The home page can be found {{ link_to_route('job.index', 'here') }} which includes seven recent jobs</p>
  <p>This application has two types of users, job seeker and employer.
    Users can create either type of account from the top of the page above the navigation.
    A job seeker can apply for jobs on the job detail page. While employers once logged in can advertise a job     and check their applications for each job in their admin area., once logged in.</p>
  <p>The job seeker sign up page can be found {{ link_to_route('user.seeker.create', 'here') }}
     while the employer home page is {{ link_to_route('user.employer.create', 'here') }}.</p>
  <p>The database can be migrated to add jobs and employers by runing <code>php artisan db:seed</code> from the      unix command line where the application is stored(root folder).    </p>
  <p>To create the database from the command line run <code>php artisan migrate</code>. to drop the databases        run <code>php artisan migrate:reset</code>.</p>
@stop