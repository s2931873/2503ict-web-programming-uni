@extends('../layout')
@section('title')
All Employers| Aero Jobs - The place for jobs!
@stop
@section('body')
<h3>All Employers</h3>
{{ $employers->links()}}
 @foreach ($employers as $employer)
  <h4>{{{ $employer->company }}}</h4>
<ul class="list-group">
  <li class="list-group-item">Industry: {{{ $employer->industry }}}</li>
  <li class="list-group-item">Description: {{{ $employer->description }}}</li>
  <li class="list-group-item">{{ link_to_route('employer.show','Employers Jobs', array($employer->id)) }}</li>
</ul>
<br/>
  @endforeach
{{ $employers->links()}}
@stop
