@extends('../layout')
@section('title')
Employer| Aero Jobs - The place for jobs!
@stop
@section('body')
@if (Auth::check())
@if(Auth::user()->employer_id == $employer_id)
<p>{{ link_to_route('job.create', 'Create a Job') }}</p>
@endif
@endif
@foreach ($job as $j)
<ul class="list-group">
   <li class="list-group-item"><h4>{{ link_to_route('job.show',$j->title, array($j->id)) }}</h4></li>
  <li class="list-group-item">Location: {{{ $j->location }}}</li>
  <li class="list-group-item">Salary: {{{ $j->salary }}}</li>
  <li class="list-group-item">Start Date: {{{ $j->start_date }}}</li>
  <li class="list-group-item">End Date: {{{ $j->end_date }}}</li>
  <li class="list-group-item">{{ link_to_route('job.show','More details', array($j->id)) }}</li>
</ul>
<br/>
  @endforeach
@stop