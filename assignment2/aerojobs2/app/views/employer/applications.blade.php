@extends('../layout')
@section('title')
Applications | Aero Jobs - The place for jobs!
@stop
@section('body')

@if (Auth::check())
@if(Auth::user()->employer_id == $employer_id)
{{ $applications->links() }}
<h2>Applications</h2>
@foreach($applications as $app)
<h4>Application from {{{$app->full_name}}}</h4>
<ul class="list-group">
   <li class="list-group-item">{{ link_to_route('user.seeker.profile','Users Profile', array($app->id)) }}</li>
   <li class="list-group-item">Application Letter: {{{$app->application_letter}}}</li>
   <li class="list-group-item">Application Date/Time: {{{$app->application_date}}}</li>  
</ul>
@endforeach
{{ $applications->links() }}
@else
<p>You are not authorized to view this page</p>
@endif
@endif
@stop