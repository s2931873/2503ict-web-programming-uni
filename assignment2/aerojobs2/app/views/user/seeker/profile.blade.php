@extends('../layout')
@section('title')
My Profile| Aero Jobs - The place for jobs!
@stop
@section('body')
     


@foreach($user as $u)
@if($u->type==1)
<h2>{{{$u->full_name}}}s Profile</h2>
  <p>Full name: {{{$u->full_name}}}</p>
  <p>Phone Number: {{{$u->phone_number}}}</p>
  <p>Email: {{{$u->email}}}</p>
<img src="{{ asset($u->image->url('medium')) }}">
@else
<p>The user is an employer</p>
@endif
@endforeach
@stop