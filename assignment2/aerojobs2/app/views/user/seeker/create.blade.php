@extends('../../layout')
@section('title')
Create Account| Aero Jobs - The place for jobs!
@stop
@section('body')
<h2>Create Login</h2>
@if (Auth::check())
         {{"You are already logged in"}}
         {{ link_to_route('user.logout', 'Log Out') }}
    @else
{{Form::open(array('route' => 'user.store', 'files' => true))}}
  {{ Form::label("username", "Username") }}
  {{ Form::text("username") }}<br/>
  {{ Form::label("email", "Email") }}
  {{ Form::text("email") }}<br/>
  {{ Form::label("phone_number", "Phone Number") }}
  {{ Form::text("phone_number") }}<br/>
  {{ Form::label("full_name", "Full name (eg. John Smith)") }}
  {{ Form::text("full_name") }}<br/>
  {{ Form::label('image', 'Image:') }}
  {{ Form::file('image') }}<br/>
  {{ Form::label("password", "Password") }}
  {{ Form::password("password") }}<br/>
  {{ Form::hidden('type', '1') }}
  {{ Form::submit("Create") }}<br/>
  {{ Form::close() }}
@endif
@if($errors->has())
   @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
  @endforeach
@endif
@stop
