<?php

class JobSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
    $job = new Job;
    $job->title = 'C# developer needed!';
    $job->location = 'Gold Coast';
    $job->description = 'We are looking for a junior with skills in ASP.NET and C# 4.0';
    $job->salary = 75000;
    $job->start_date = "15-Feb-2014";
    $job->end_date = "21-Mar-2014";
    $job->employer_id = 1;
    $job->save();
    
    $job = new Job;
    $job->title = 'c++ developer needed!';
    $job->location = 'Brisbane';
    $job->description = 'We are looking for a junior with skills in C++ and OO programing';
    $job->salary = 85000;
    $job->start_date = "15-Feb-2009";
    $job->end_date = "10-Sep-2014";
    $job->employer_id = 2;
    $job->save();
    
    $job = new Job;
    $job->title = 'MYOB Help desk';
    $job->location = 'Sydney';
    $job->description = 'Doing accountant stuff';
    $job->salary = 70000;
    $job->start_date = "15-Feb-2014";
    $job->end_date = "21-Oct-2014";
    $job->employer_id = 3;
    $job->save();
    
     $job = new Job;
    $job->title = 'Film Actor';
    $job->location = 'Sydney';
    $job->description = 'Acting';
    $job->salary = 90000;
    $job->start_date = "17-Jan-2014";
    $job->end_date = "24-Dec-2014";
    $job->employer_id = 3;
    $job->save();
	}
}
