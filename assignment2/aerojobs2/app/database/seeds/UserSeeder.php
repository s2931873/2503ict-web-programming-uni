<?php

class UserSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
/*
 $table->string('username')->unique();
     $table->string('password')->index();
     $table->string('email')->unique();
     $table->string('phone_number');
     $table->string('full_name');
     $table->integer('type');
     $table->integer('employer_id')->nullable();
     $table->string('remember_token');
*/
		$user = new User;
    $user->username = 'userone';
    $user->password = 'letmein';
    $user->email = 'user@aerojobs.com';
    $user->phone_number = '55297712';
    $user->full_name = "Winston Smith";
    $user->type=2;
    $user->employer_id = 1;
    $user->remember_token = 'default';
    $user->save();
    
    $user = new User;
    $user->username = 'usertwo';
    $user->password = 'letmein';
    $user->email = 'usertwo@aerojobs.com';
    $user->phone_number = '555297712';
    $user->full_name = "John Citizen";
    $user->type=2;
    $user->employer_id = 2;
    $user->remember_token = 'default';
    $user->save();
    
    	$user = new User;
    $user->username = 'userthree';
    $user->password = 'letmein';
    $user->email = 'userthree@aerojobs.com';
    $user->phone_number = '5529745712';
    $user->full_name = "Lisa Ryans";
    $user->type=2;
    $user->employer_id = 3;
    $user->remember_token = 'default';
    $user->save();
	}

}
