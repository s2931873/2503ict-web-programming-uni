<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
    $this->call('JobSeeder');
		$this->call('EmployerSeeder');
    $this->call('UserSeeder');
	}

}
