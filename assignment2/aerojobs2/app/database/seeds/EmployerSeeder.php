<?php

class EmployerSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
    
    $employer = new Employer;
    $employer->company = 'Stark Industries';
    $employer->industry = 'Enginering';
    $employer->description = 'We have a hulk';
    $employer->remember_token = 'default';
    $employer->save();
    
    $employer = new Employer;
    $employer->company = 'Marillac Ltd.';
    $employer->industry = 'Primary Industries';
    $employer->description = 'Mining industry';
    $employer->remember_token = 'default';
    $employer->save();
    
    $employer = new Employer;
    $employer->company = 'Fox Enterprises';
    $employer->industry = 'IT';
    $employer->description = 'We are fox enterises, an umbrella corporation with 10,000 international staff members';
    $employer->remember_token = 'default';
    $employer->save();
	}

}
