<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		 Schema::create('applications', 
    function($table)
     {
       $table->increments('id');
       $table->integer('job_id');
       $table->integer('seeker_id');
       $table->string('application_letter');
       $table->datetime('application_date');
       $table->string('remember_token');
       $table->timestamps();
     });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('applications');
	}

}
