<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::create('jobs', 
    function($table)
     {
       $table->increments('id');
       $table->string('title');
       $table->string('location');
       $table->text('description');
       $table->double('salary');
       $table->datetime('start_date');
       $table->datetime('end_date');
      $table->integer('employer_id');
       $table->timestamps();
     });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	Schema::drop('jobs');
	}

}
