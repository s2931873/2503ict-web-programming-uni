<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

    //home page set
    Route::get('/', array('as'=>'home', 'uses'=> 'JobController@index'));
    //set login and logout routes as well as user store
    Route::post('user/login', array('as' => 'user.login', 'uses' =>       'UserController@login'));
    Route::post('user/store', array('as' => 'user.store', 'uses' =>       'UserController@store'));
    Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout'));
    //get users profile
    Route::get('user/seeker/profile/{id}', array('as' => 'user.seeker.profile', 'uses' => 'UserController@profile'));
    Route::get('job/list', array('as' => 'job.list', 'uses' => 'JobController@listall'));
    Route::get('job/delete', array('as' => 'job.delete', 'uses' => 'JobController@delete'));
    Route::get('employer/all', array('as' => 'employer.all', 'uses' => 'EmployerController@index'));
    Route::get('employer/applications/{id}/{employer_id}', array('as' => 'employer.applications', 'uses' => 'EmployerController@applications'));
    Route::get('job/apply/{id}', array('as' => 'job.apply', 'uses' => 'JobController@apply'));
    Route::post('job/apply/store', array('as' => 'job.apply.store', 'uses' =>         'JobController@application_store'));
    Route::get('job/search/', array('as' => 'job.search', 'uses' =>       'JobController@search'));
    Route::get('user/seeker/create', array('as' => 'user.seeker.create', 'uses' => 'UserController@create_seeker'));
    Route::get('user/employer/create', array('as' => 'user.employer.create', 'uses' => 'UserController@create_employer'));

    //register controllers
    Route::resource('docs', 'HomeController@docs');
    Route::resource('user', 'UserController');
    Route::resource('job', 'JobController');
    Route::resource('employer', 'EmployerController');

  