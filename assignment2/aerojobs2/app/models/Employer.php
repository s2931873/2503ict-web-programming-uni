<?php
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
class Employer extends Eloquent
{
  public function user()
    {
        return $this->hasOne('user');
    }
  
  public function job()
    {
        return $this->hasMany('job');
    }
  
   public function application()
    {
        return $this->hasMany('application');
    }

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'employers';

   public static $rules = array(
 'company' => 'required|unique:employers',
 'industry' => 'required',
 'description' => 'required|min:5'
);
}