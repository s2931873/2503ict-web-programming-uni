<?php
class Job extends Eloquent
{
  
   public static $rules = array(
 'location' => 'required',
 'description' => 'required|min:10',
 'salary' => 'required',  
 'start_date' => 'date',
 'end_date' => 'date'
);

  
function employer()
{
return $this->belongsTo('employer', 'employer_id');
}
  
function application()
{
return $this->hasMany('job');
}
  	protected $table = 'jobs';
}