@extends('../../layout')
@section('title')
Create Employer Account| Aero Jobs - The place for jobs!
@stop
@section('body')
<h2>Create Employer Login</h2>
<br/>
@if (Auth::check())
         {{"You are already logged in"}}
         {{ link_to_route('user.logout', 'Log Out') }}
    @else
{{Form::open(array('route' => 'user.store', 'files' => true))}}
{{ Form::label("username", "Username") }}
  {{ Form::text("username") }}<br/>
  {{ Form::label("email", "Email") }}
  {{ Form::text("email") }}<br/>
  {{ Form::label("phone_number", "Phone Number") }}
  {{ Form::text("phone_number") }}<br/>
  {{ Form::label("full_name", "Full name (eg. John Smith)") }}
  {{ Form::text("full_name") }}<br/>
  {{ Form::label("company", "Company Name") }}
  {{ Form::text("company") }}<br/>
  {{ Form::label("industry", "Industry") }}
  {{ Form::text("industry") }}<br/>
  {{ Form::label("description", "Description") }}
  {{ Form::text("description") }}<br/>
  {{ Form::label('image', 'Image:') }}
  {{ Form::file('image') }}<br/>
  {{ Form::label("password", "Password") }}
  {{ Form::password("password") }}<br/>
  {{ Form::hidden('type', 2) }}
  {{ Form::submit("Create") }}
  {{ Form::close() }}
@endif
@if($errors->has())
   @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
  @endforeach
@endif

@if($errors->has())
   @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
  @endforeach

  @endif

@stop
