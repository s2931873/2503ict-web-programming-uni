<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
   <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{$path}}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="{{$path}}/css/bootstrap-theme.css" rel="stylesheet" type="text/css"/>
    <link href="{{$path}}/css/style.css" rel="stylesheet" type="text/css"/>
    <link rel="icon" type="image/x-icon" href="{{$path}}/favicon.ico" />
   
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="{{$path}}/js/bootstrap.js"></script>
  </head>
  <body>
<div class="container">
      <header id="logo">
      <div class="jumbotron ">
        <h1>Aero Jobs</h1>
      <p>The place for jobs!</p>
      </div>
      
        <div class="col-xs-3 col-md-8 " id="breadcrumbs">
          <p>@yield('breadcrumb')
          
          </p>
          </div>
        <div class="well">
    @if (Auth::check())
          <ul>
          <li class="list-group-item">
            {{"You are logged in"}}
         {{ Auth::user()->full_name }}
          {{ link_to_route('user.logout', 'Log Out') }}<br/>
          @if( Auth::user()->type == 2)
            <li class="list-group-item">{{ link_to_route('employer.show','Go to my admin area', array(Auth::user()->employer_id)) }}</li>
          @endif
            </ul>
    @else
      {{Form::open(array('route' => 'user.login'))}}
        {{ Form::label("username", "Username") }}
        {{ Form::text("username") }}
        {{ Form::label("password", "Password") }}
        {{ Form::password("password") }}
        {{ Form::submit("Login") }}
        {{ Form::close() }}  
        {{ link_to_route('user.seeker.create', 'Create a Job Seeker Account') }}
          |
        {{ link_to_route('user.employer.create', 'Create an Employer Account') }}
    @endif
        </div>
       <nav class="navbar navbar-default" role="navigation">
       
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
        <li>{{ link_to_route('job.index', 'Home') }}</li>
        <li>{{ link_to_route('job.list', 'All Jobs') }}</li>
         <li>{{ link_to_route('employer.all', 'All Employers') }}</li>
      <li>{{ link_to_route('job.create', 'Advertise a Job') }}</li>
        <li><a href="/public/documentation.html">Documentation</a></li>
      </ul>
      <form class="navbar-form navbar-left" method="get" action="search.php"  role="search">
        <div class="form-group">
          <input name="query" type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
      
        
      </header>
      
      <div class="container-fluid">
        <div id="left" class="col-sm-3 col-md-4 ">
          <ul class="nav nav-pills nav-stacked well">
            <li>{{ link_to_route('job.index', 'Home') }}</li>
            <li>{{ link_to_route('job.list', 'All Jobs') }}</li>
            <li>{{ link_to_route('employer.all', 'All Employers') }}</li>
            <li>{{ link_to_route('job.create', 'Advertise a Job') }}</li>
            <li><a href="search.php"><span class="glyphicon glyphicon-search"> </span>Search for Jobs</a></li>
            <li><a href="documentation.html">Documentation</a></li>
          </ul>
        </div>
  <div id="right" class="col-xs-3 col-md-8 ">
  @yield('body')
    </div>
 <div class="container-fluid">
       <div class="col-xs-3 col-md-12 well" id="footer">
        <p>Copyright AeroJobs.com - 2013 - 2014</p>
        <p>Created by Matthew de Marillac | s2931873</p>
      </div>
  </div>
  </div>
  </body>
  </html>