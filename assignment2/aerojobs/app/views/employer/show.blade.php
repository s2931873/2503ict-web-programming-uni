@extends('../layout')
@section('title')
Employer| Aero Jobs - The place for jobs!
@stop
@section('body')
@if (Auth::check())
@if(Auth::user()->employer_id == $employer_id)
<h3>My Jobs</h3>
<p>{{ link_to_route('job.create','Create a Job!') }}</p>
 @foreach ($job as $j)
<ul class="list-group">
   <li class="list-group-item"><h4>{{ link_to_route('job.show',$j->title, array($j->id)) }}</h4></li>
  <li class="list-group-item">Location: {{{ $j->location }}}</li>
  <li class="list-group-item">Salary: {{{ $j->salary }}}</li>
  <li class="list-group-item">Start Date: {{{ $j->start_date }}}</li>
  <li class="list-group-item">End Date: {{{ $j->end_date }}}</li>
    {{ Form::close() }}</li>
</ul>
<br/>
  @endforeach
<p>{{ link_to_route('job.create','Create a Job!') }}</p>
@else
{{"Sorry, you are not authorized to view this page"}}
@endif
@else
{{"Sorry, you are not authorized to view this page"}}
@endif

@stop