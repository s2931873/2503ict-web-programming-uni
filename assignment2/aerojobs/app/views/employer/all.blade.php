@extends('../layout')
@section('title')
All Employers| Aero Jobs - The place for jobs!
@stop
@section('body')
<h3>All Employers</h3>
{{ $employers->links()}}
 @foreach ($employers as $employer)
<ul class="list-group">
  <li class="list-group-item"><h4>{{{ $employer->company }}}</h4></li>
  <li class="list-group-item">Location: {{{ $employer->location }}}</li>
  <li class="list-group-item">Salary: {{{ $employer->industry }}}</li>
  <li class="list-group-item">Start Date: {{{ $employer->description }}}</li>
  <li class="list-group-item">{{ link_to_route('employer.show','Employers Jobs', array($employer->id)) }}</li>
</ul>
<br/>
  @endforeach
{{ $employers->links()}}
@stop
