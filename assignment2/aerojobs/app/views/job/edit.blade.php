@extends('../layout')
@section('title')
Edit | {{{ $jobs->title }}}
@stop
@section('body')
@if (Auth::check())
@if(Auth::user()->employer_id == $jobs->employer_id)
<h2>Edit {{{ $jobs->title }}}</h2>
{{Form::model($jobs, array('method' => 'PUT', 'route' => 
array('job.update', $jobs->id))) }}
{{ Form::label('title', 'Job Ttile: ') }}<br/>
{{ $errors->first('title') }}<br/>
{{ Form::text('title') }}<br/>
{{ Form::label('salary', 'Salary: ') }}<br/>
{{ $errors->first('salalry') }}<br/>
{{ Form::text('salary') }}<br/>
{{ Form::label('industry', 'Industry: ') }}<br/>
{{ $errors->first('industry') }}<br/>
{{ Form::text('industry') }}<br/>
{{ Form::label('description', 'Description: ') }}<br/>
{{ $errors->first('description') }}<br/>
{{ Form::text('description') }}<br/>
{{ Form::label('start_date', 'Start Date: ') }}<br/>
{{ $errors->first('start_date') }}<br/>
{{ Form::text('start_date') }}<br/>
{{ Form::label('end_date', 'End Date: ') }}<br/>
{{ $errors->first('end_date') }}<br/>
{{ Form::text('end_date') }}<br/>
{{ Form::hidden('employer_id', $jobs->employer_id) }}
{{ Form::submit('Update') }}
{{ Form::close() }}
@endif
@else
{{"Sorry, you are not authorized to view this page."}}

@endif
@stop