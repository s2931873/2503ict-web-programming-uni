@extends('../layout')
@section('title')
{{{ $jobs->title }}} | Aero Jobs - The place for jobs!
@stop
@section('body')

<h2>{{{ $jobs->title }}}</h2>
<p>Location: {{{ $jobs->location }}}</p>
<p>Description: <br/>
  {{{ $jobs->description }}}</p>
<p>Salary: {{{ $jobs->salary }}}</p>
<p>Start Date: {{{ $jobs->start_date }}}</p>
<p>End Date: {{{ $jobs->end_date }}}</p>
@if (Auth::check())
@if(Auth::user()->employer_id == $jobs->employer_id)
  <p>{{ link_to_route('job.edit','Edit Job!', array($jobs->id)) }}</p>
  <p>{{ Form::open(array('method' => 'DELETE', 'route' => array('job.destroy', $jobs->id))) }}
    {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}</p>
      <p>View job applications</p>

@endif
@else

<p>{{ link_to_route('job.apply','Apply for job!', array($jobs->id)) }}</p>

@endif

@stop