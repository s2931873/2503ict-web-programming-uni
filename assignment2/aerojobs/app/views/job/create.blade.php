@extends('../layout')
@section('title')
Create Job
@stop
@section('body')

@if (Auth::check())
@if(Auth::user()->type == 2)
<p>Welcome, create a job bellow!</p>
{{ Form::open(array('action' => 'JobController@store')) }}
{{ Form::label('title', 'Job Ttile: ') }}<br/>
{{ $errors->first('title') }}<br/>
{{ Form::text('title') }}<br/>
{{ Form::label('location', 'Location: ') }}<br/>
{{ $errors->first('location') }}<br/>
{{ Form::text('location') }}<br/>
{{ Form::label('salary', 'Salary: ') }}<br/>
{{ $errors->first('salalry') }}<br/>
{{ Form::text('salary') }}<br/>
{{ Form::label('industry', 'Industry: ') }}<br/>
{{ $errors->first('industry') }}<br/>
{{ Form::text('industry') }}<br/>
{{ Form::label('description', 'Description: ') }}<br/>
{{ $errors->first('description') }}<br/>
{{ Form::text('description') }}<br/>
{{ Form::label('start_date', 'Start Date: ') }}<br/>
{{ $errors->first('start_date') }}<br/>
{{ Form::text('start_date') }}<br/>
{{ Form::label('end_date', 'End Date: ') }}<br/>
{{ $errors->first('end_date') }}<br/>
{{ Form::text('end_date') }}<br/>
{{ Form::hidden('employer_id',Auth::user()->employer_id ) }}
{{ Form::submit('Create') }}
{{ Form::close() }}
@else
{{"You must be logged in as an employer to view this page."}}

@endif
@else
{{"You must be logged in as an employer to view this page."}}

@endif
@stop