<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::create('employers', 
    function($table)
     {
       $table->increments('id');
       $table->string('company')->unique();;
       $table->string('industry');
       $table->text('description');
       $table->string('remember_token');
       $table->timestamps();
     });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('employers');
	}

}
