<?php

class JobSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
    $job = new Job;
    $job->title = 'C# developer needed!';
    $job->location = 'Gold Coast';
    $job->industry = 'IT';
    $job->description = 'We are looking for a junior with skills in ASP.NET and C# 4.0';
    $job->salary = 75000;
    $job->start_date = "15-Feb-2014";
    $job->end_date = "21-Mar-2014";
    $job->employer_id = 1;
    $job->save();
    
    $job = new Job;
    $job->title = 'c++ developer needed!';
    $job->location = 'Brisbane';
    $job->industry = 'IT';
    $job->description = 'We are looking for a junior with skills in C++ and OO programing';
    $job->salary = 85000;
    $job->start_date = "15-Feb-2009";
    $job->end_date = "15-Feb-2009";
    $job->employer_id = 2;
    $job->save();
	}
}
