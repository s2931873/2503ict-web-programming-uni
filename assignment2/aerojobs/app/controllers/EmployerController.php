<?php

class EmployerController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$employers = Employer::paginate(15);
    return View::make('employer.all', compact('employers'));
	}
  
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
     $job = Job::where('employer_id', '=', $id)->get();
     return View::make('employer.show', compact('job'))->With('employer_id', $id);
}
}