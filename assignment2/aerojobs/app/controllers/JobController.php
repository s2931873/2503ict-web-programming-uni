<?php

class JobController extends \BaseController {

  
  
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$jobs = Job::take(10)->get();
    return View::make('job.index', compact('jobs'));
	}
  
/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function listall()
	{
		$jobs = Job::paginate(15);
    return View::make('job.list', compact('jobs'));
	}
 
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	  return View::make('job.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    
		$input = Input::all();
    //lets make sure this is saved to the correct employer
    if(Auth::user()->employer_id == $input['employer_id']){
    //lets save the job
    $job = new Job();
    $job->title = $input['title'];
    $job->location = $input['location'];
    $job->description = $input['description'];
    $job->salary = $input['salary'];
    $job->industry = $input['industry'];
    $job->start_date = $input['start_date'];
    $job->end_date = $input['end_date'];
    $job->employer_id = $input['employer_id'];
    $job->save();
    return Redirect::route('employer.show', $job->id);
  }else{
      //unfortunately the job was trying to be saved to the wrong employer
  return Redirect::route('job.create')->With("You are not saving to the correct employer,
  please do not change hidden inputs!");
  }
}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
     $jobs = Job::find($id);
    
     return View::make('job.show', compact('jobs'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	$jobs = Job::find($id);
  return View::make('job.edit', compact('jobs'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
    $job = Job::find($id);
    $input = Input::all();
    $job->title = $input['title'];
    $job->location = $input['title'];
    $job->description = $input['description'];
    $job->salary = $input['salary'];
    $job->industry = $input['industry'];
    $job->start_date = $input['start_date'];
    $job->end_date = $input['end_date'];
    $job->employer_id = $input['employer_id'];
    $job->save();
     return View::make('employer.all');
  }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		 $job = Job::find($id);
    //check this job belongs to user
    if(Auth::user()->employer_id == $job->employer_id){
     $job->delete();
   return Redirect::route('job.list');
        }
    else{
      //if it doesnt redirect with errors
      echo "fuu";
    }
	}

  public function apply($id)
	{
		 $job = Job::find($id);
    //check this job belongs to user
     return View::make('job.apply', compact('job'));
  }
  
    public function application_store()
	{
		 //
  }
  
  public function search(){

    $q = Input::get('myInputField');
    $searchTerms = explode(' ', $q);
    $query = DB::table('products');
    
    foreach($searchTerms as $term)
    {
        $query->where('name', 'LIKE', '%'. $term .'%');
    }
    $results = $query->get();    
  }
}