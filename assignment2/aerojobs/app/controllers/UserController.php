<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
	//
	}
 
    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
  public function create_seeker()
	{
  return View::make('user.seeker.create');
	}

  
  public function create_employer()
	{
  return View::make('user.employer.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
     $input = Input::all();
    $v = Validator::make($input, User::$rules);
       
    $image =Input::file('image');
    if($input['type'] == 2){
         //now we store employer details
        $employer = new Employer;
       $employer->company = $input['company'];
       $employer->industry = $input['industry'];
       $employer->description = $input['description'];
       $employer->remember_token = "default";
        $employer->save();
       // $user->save();
      }
    
       $password = $input['password'];
       $encrypted = Hash::make($password);
       $user = new User;
       $user->username = $input['username'];
       $user->password = $encrypted;
       $user->email = $input['email'];
       $user->phone_number = $input['phone_number'];
       $user->full_name = $input['full_name'];
       $user->photo = "none";
       $user->type= $input['type'];
       $user->remember_token = "default";
    if($input['type'] == 2){
       $account = Employer::find($employer->id);
       $user->employer()->associate($account);
    }
       $user->save();  
    // $user->employer_id = $employer->id;
       
      
     return Redirect::route('job.index');
     }
    
    

  /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
  public function login(){
    $credentials = array(
  'username' => Input::get('username'),
  'password' => Input::get('password')
  );
   if (Auth::attempt($credentials)) {
   
    return Redirect::to(URL::previous());
    }else{
   }
  }
  /**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
  public function logout(){
    Auth::logout();
      return Redirect::to(URL::previous());  
    }
  
  
  
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
