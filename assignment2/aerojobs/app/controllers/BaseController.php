<?php

class BaseController extends Controller {
public function __construct()
  {                   
    // Share a var with all views
    View::share('path', URL::to(''));
  }
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
